# list of functions included
#
# runKMeans()
# runKMeans2()
# runKMeans3()
# fitKModel()
# transformKModel()
# computeCostKM()
# displayKMSummary()
# calculateKMClusters()
# evaluateKM()
# evaluateKM2()
# evaluateKM3()
# createClusters()
# runEvalKM()
# runEvalKM2()
# runEvalKM3()
# calcDistance()
# calcDistance2()
# chooseOptimalK()
# createDFLabeledClusters()
# storePreProcessedETList()
# createWaitList()
# getETList()
# createETFPathsList()
#

#
def runKMeans(df,k):
  """
    KMeans() on scaled features
  """
  # local variables
  #
  # rational
  kmModel = KMeans(featuresCol="scaledFeatures", seed=1)\
                   .setK(k)\
                   .setInitSteps(2)\
                   .setMaxIter(80)\
                   .fit(df)
  #
  # return variable
  return kmModel
#
# end of runKMeans()
#

#
def runKMeans2(df,k):
  """
    KMeans() on PCA features
  """
  # local variables
  #
  # rational
  kmModel = KMeans(featuresCol="pcaFeatures", seed=1)\
                   .setK(k)\
                   .setInitSteps(2)\
                   .setMaxIter(80)\
                   .fit(df)
  #
  # return variable
  return kmModel
#
# end of runKMeans2()
#

#
def runKMeans3(df, k, initSteps, maxIter):
  """
   KMeans() on PCA features
   define initSteps and max iterations as parameters
   the model is returned without fitting the data
  """
  # local variables
  #
  # rational
  kmModel = KMeans(featuresCol = "pcaFeatures", seed=1)\
                   .setK(k)\
                   .setInitSteps(2)\
                   .setMaxIter(100)
  #
  # return variable
  return KMean
#
# end of runKMeans3()
#

#
def fitKModel(KMean,df):
  """
    function fitKMModel() fits a KMeans model using a df
  """
  # local variables
  #
  # rational
  kmModel=kMean.fit(df)
  #
  # return variable
  return kmModel
#
# end of runKMeans()
#

#
def transformKModel(kmModel,df):
  """
    function transformKMModel() transforms a KMeans model using a df
  """
  # local variables
  #
  # rational
  kmModelT=kmModel.transform(df)
  #
  # return variable
  return kmModelT
#
## end of transformKModel()
#

#
def computeCostKM(kmModel,df):
  """
    function computeCost() calculates the WCSSE for a KMModel run on a dataset
  """
  # local variables
  #
  # rational
  wcsse=kmModel.computeCost(df)
  #
  # return value
  return wcsse
#
## end of computeCost()
#

#
def displayKMSummary(kmModel):
  """
    funcion displayKMSummary() displays a summary info for a KMmodel
  """
  # local variables
  #
  # rational
  centers = kmModel.clusterCenters()
  print('Number of clusterCenters:',len(centers))
  print('clustersizes:',kmModel.summary.clusterSizes)
  print("Cluster Centers: ")
  #
  for center in centers:
    print(center)
  
  #
#
# end of displayKMSummary()
#

#
def calculateKMClusters(kmModelT):
  """
    function calculateKMClusters() calculates clusters for a KMmodel
  """
  # local variables
  #
  # rational
  clusters=kmModelT.select('prediction')
  clusters.show()
  #
  # return value
  return clusters
#
# end of calculateKMClusters()
#

#
def evaluateKM(df, k):
  """
    function evaluateKM() evaluates the KMeans|| model
  """
  # rational
  kmModel = runKMeans(df,k)
  #
  wcsse = computeCostKM(kmModel,df)
  #
  # return value
  return wcsse
#
# end of evaluateKM()
#
 
#
def evaluateKM2(df,k):
  """
    function evaluateKM2() evaluates the KMeans|| model
    it calle get_silhouette_graph2() to calculate and save the silhouette graphs
  """
  # rational
  kmModel = runKMeans(df,k)
  #
  kmModelT = transformKModel(kmModel,df)
  #
  get_silhouette_graph2(kmModelT)
  #
  wcsse = computeCostKM(kmModel,df)
  #
  # return value
  return wcsse
#
# end of evaluateKM2()
#

#
def evaluateKM3(df,k):
  # rational
  kmModel = runKMeans(df,k)
  #
  wcsse = computeCostKM(kmModel,df)
  #
  # return value
  return wcsse
#
# end of evaluateKM4()
#

# 
def createClusters(df):
  """
    function createClusters() creates a list of clusters given a df
  """
  # local variables
  clusters=[]
  #
  # rational
  clustersNo = df.select('prediction').distinct().count()
  #
  for i in range(clustersNo):
    dfName=str(i)
    tmpDict=dict([(dfName,dfName)])
    # 
    for key,v1 in tmpDict.items():
      v1 = df.filter(df.prediction==i)   
      clusters.append([dfName,v1])  
    #
  #
  # return variable
  return clusters
#
# end of createClusters()
#

#
def runEvalKM(df):
  """
    function runEvalKM() runs evaluation on KMeans||
    by using a set of different number of clusters
    it calls evaluateKM()
  """
  # local variables
  params = [2,3,4,5,6,7,8,9,10,11,12]
  #
  # rational
  metrics = [evaluateKM(df, param) for param in params]
  print(params)
  print(metrics)
  #
  plt.plot(params, metrics)
  fig = plt.gcf()
  fig.show()
#
# end of runEvalKM()
#

#
def runEvalKM2(df):
  """
    function runEvalKM() runs evaluation on KMeans||
    by using a set of different number of clusters
    it calls evaluateKM2()
  """
  # local variables
  params = [2,3,4,5,6,7,8,9,10,11,12]
  #
  # rational
  metrics = [evaluateKM2(df, param) for param in params]
  print(params)
  print(metrics)
  #
  plt.plot(params, metrics)
  fig = plt.gcf()
  fig.show()
#
# end of runEvalKM2()
#

#
def runEvalKM3(df):
  """
    function runEvalKM3() runs evaluation on KMeans||
    no plots are produced
  """
  # local variables
  params = [2,3,4,5,6,7,8,9,10,11,12]
  #
  # rational
  metrics = [[param,evaluateKM4(df, param)] for param in params]
  #
  return metrics
#
## end of runEvalKM3()
#

# 
def calcDistance(df,dfModel):
  """
    calculate the distance of a point from the cluster centroid
    using scaled features
  """
  # local variables
  distances = []
  centers   = dfModel.clusterCenters()
  #
  # rational
  data = df.select('scaledFeatures')\
	       .rdd.map(lambda row : row[0])\
	       .collect()
  
  #
  labels = df.select('prediction')\
	         .rdd.map(lambda row: row[0])\
	         .collect()
  
  #  
  for i in range(len(data)):
    dist = data[i] - centers[labels[i]]
    #
    distance = np.linalg.norm(dist)
    #
    distances.append((data[i],float(distance)))
    #
  
  #return distances
  distDF = spark.createDataFrame(distances,('scaledFeatures','distance'))
    #
  dfNew = df.join(distDF,'scaledFeatures','inner')
  #
  return dfNew
#

#
def calcDistance2(df):
  """
    calculate the distance of a point from the cluster centroid
    using pca features
  """
  # local variables
  distances = []
  centers   = dfModel.clusterCenters()
  #
  # rational
  data = df.select('pcaFeatures')\
	       .rdd.map(lambda row : row[0])\
	       .collect()
  #
  labels = df.select('prediction')\
	         .rdd.map(lambda row: row[0])\
	         .collect()
  #  
  for i in range(len(data)):
    dist = data[i] - centers[labels[i]]
    #
    distance = np.linalg.norm(dist)
    #
    distances.append((data[i],float(distance)))
    #
  
  #return distances
  distDF = spark.createDataFrame(distances,('pcaFeatures','distance'))
    #
  dfNew = df.join(distDF,'pcaFeatures','inner')
  #
  return dfNew
#

#
def chooseOptimalK(listIn, pos):
  # local variables
  # k = listIn[pos][0]
  flag = True 
  # rational
  #
  # calculate diffs between the three consecutive pairs of costs
  d1 = (listIn[pos][1]   - listIn[pos+1][1]) / listIn[pos][1]
  d2 = (listIn[pos+1][1] - listIn[pos+2][1]) / listIn[pos+1][1]
  d3 = (listIn[pos+2][1] - listIn[pos+3][1]) / listIn[pos+2][1]
  #
  # calculate combined diffs
  d12 = d1 - d2
  d23 = d2 - d3
  #
  # check if elbow has been found
  # otherwise, shift pos one place to the right of the list
  while pos+4 <= len(listIn) and flag == True:
    if d12 > d23:
      k = listIn[pos+1][0]
    else:
      pos = pos + 1
      k = chooseOptimalK2(listIn, pos)
    flag = False    
  #
  # return values
  return k
#
# end of chooseOptimalK()
#

# 
def createDFLabeledClusters(df, eT, writePath):
  """
    function createDFLabeledclusters() creates clusters and labels events in a df
  """
  # local variables
  dfClusters = []
  clusters   = [] 
  #
  # rational
  clusterNums = df[1].select('cluster')\
                     .distinct()\
                     .collect()
  #
  clusters = [c[0] for c in clusterNums]
  #
  print('clusterNums: ',clusterNums)  
  #
  for cl in clusters:
    #
    dfClust = updLabel1(df[1],cl)
    #
    print('cluster ', cl, 'has been labeled')
    dfName = df[0] + '-' + str(cl)
    #
    dfClust.write.parquet(writePath  + dfName)
    #
    dfClusters.append([eT, dfName, dfClust]) 
    #
  #
  return dfClusters
  #
# end of createDFLabeledClusters()
#

#
def storePreProcessedETList(ETListAll, writePath):
  #
  ctr = 0
  #
  for df in ETListAll:
    dfName = df[0]
    #
    df[1].write.parquet(writePath + dfName)
    #
  #
#

#
def createWaitList(pathName):
  # local variables
  dfWaitingList = []
  #
  # rational
  dfWaitingList = createDFList(pathName)
  #  
  return dfWaitingList
  #
#

#
def getETList(clustETListIn):
  #
  eTList = []
  # create entities list
  for c in clustETListIn:
    match1 = re.search(r'[\.0-9]+',c)
    index1 = match1.group(0)
    #
    if index1 not in eTList:
      eTList.append(index1)
    #
  #
  return eTList
  #
#

#
def createETFPathsList(path, eTList, clustETListIn):
  #
  filesWList = []
  # create entity/fPaths list
  for e in eTList:
    #
    flag = False
    fTmpList = []
    #
    for c in clustETListIn:
      #
      if c.startswith(e):
        flag = True
        #
        tmpPath = path + e + '/' + c
        fTmpList.append(tmpPath)
        #
      #
      else:
        pass
      #
    #    
    if flag:
      filesWList.append(fTmpList)
    else:
      pass 
      #
    #
  #
  return filesWList  
#



