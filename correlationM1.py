# list of functions included
#
# calculatePearsonCorrelation()
# displayCorrList()
#

#
def calculatePearsonCorrelation(df):
  """
    function calculatePearsonCorrelation() calculates Pearson correlation among idx_fields
  """
  # local variables
  # initialize a list of correlation
  correlationList = []
  # list of negative correlations
  negCorrelation = []
  #  sublists fo negative correlations
  #    high (abs(cor)>0.5)
  #    medium (abs(cor)>0.3 and <=0.5)
  #    low (abs(cor)>0.1 and <=0.3)
  highNegCor = []
  medNegCor  = []
  lowNegCor  = []
  #
  # list of positive correlations
  posCorrelation = []
  #  sublists fo positive correlations
  #    high (cor>0.5)
  #    medium (cor>0.3 and <=0.5)
  #    low (cor>=0.1 and <=0.3)
  highPosCor = []
  medPosCor = []
  lowPosCor = []
  #
  # list of zero or nearly zero correlationss
  # (abs(cor)<0.1)
  zeroCorrelation = []
  #
  # local constants
  # setting correlation thresholds
  highThreshold   = 0.5
  mediumThreshold = 0.3
  lowThreshold    = 0.1
  zero            = 0.0
  #
  # rational
  #
  # calculate correlations and separate them into subGroups
  #
  for i,col1 in enumerate(df.columns):
    if col1.startswith('idx'):
      for j,col2 in enumerate(df.columns):
        if col2.startswith('idx'):
          if col1 != col2 and\
             i<j:
            correlation = df.corr(col1,col2)
            #print(col1,col2,'correlation:',correlation)
            #
            # working with negative correlations
            if correlation < zero:
              if (abs(correlation) > highThreshold):
                highNegCor.append([col1, col2, correlation])
              elif (abs(correlation) > mediumThreshold):
                medNegCor.append([col1, col2, correlation])
              elif (abs(correlation) > lowThreshold):
                lowNegCor.append([col1, col2, correlation])
            #
            # working with positive correlations
            if correlation > zero:
              if (correlation > highThreshold):
                highPosCor.append([col1, col2, correlation])
              elif (correlation > mediumThreshold):
                medPosCor.append([col1, col2, correlation])
              elif (correlation > lowThreshold):
                lowPosCor.append([col1, col2, correlation])    
            #
            # working with zero or nearly zero correlations
            elif (abs(correlation) < lowThreshold):
              zeroCorrelation.append([col1, col2, correlation])
            #  
  # append sublists to the main lists of correlations
  # negative sublists
  negCorrelation.append(highNegCor)
  negCorrelation.append(medNegCor)
  negCorrelation.append(lowNegCor)
  # positive sublists
  posCorrelation.append(highPosCor)
  posCorrelation.append(medPosCor)
  posCorrelation.append(lowPosCor)      
  # update the main correlation List
  correlationList.append(negCorrelation)    
  correlationList.append(posCorrelation)
  correlationList.append(zeroCorrelation)
  #        
  # return value
  return correlationList
  #
#
# end of calculatePearsonCorrelation()
#
 
#
def displayCorrList(corrList):
  """
    function displayCorrList() displays all the correlation papers in a correlation list
  """
  #
  # local variables
  corrTypes = ['Negative correlation',\
               'Positive correlation',\
               'Zero / Nearly-Zero correlation']
  #
  corrLevels = ['HIGH','MEDIUM','LOW']
  #
  # rational
  #
  print('Correlation Analysis')
  print('--------------------')
  #
  for idx1,cor in enumerate(corrList):
    cType = ''
    if idx1 == 0:
      cType = corrTypes[0] # Negative correlation
    elif idx1==1:
      cType = corrTypes[1] # Positive correlation
    else:
      cType = corrTypes[2] # Zero / Nearly-Zero correlation
    #
    print('\t',cType)
    print('\t-----------------------')
    #
    for idx2, j in enumerate(cor):
      if cType == corrTypes[0] or\
         cType == corrTypes[1]:
        #
        level = ''
        #
        if idx2 == 0:
          level = corrLevels[0] # HIGH
        elif idx2 == 1:
          level = corrLevels[1] # MEDIUM
        else:
          level = corrLevels[2] # LOW
        #
        print('\t',level)
        print('\t=======')
        #
        if j == []:
          print('\t\tNone')
        #  
        for idx3,k in enumerate(j):
          print('\t\t',idx3,':',k)
      #
      else: # Zero/Nearly-Zero correlation
        if j == []:
          print('\t\tNone')
        else:
          print('\t\t',idx2,':',j)   
    print('\n')
  #
# end of displayCorrList()
#