# list of functions included
#
# syslogPatternDef()

#
def syslogPatternDef(inLog, writePath):
  """
    define the regex pattern for a syslog message
    then, extract the fields from the input rec
  """
  #
  pattern = '.* ((?:\d{1,3}\.){3}\d{1,3}) ([A-Za-z0-9]+)\[(\d+)\]: (.*) client (.*)#(\d+): ([A-Z]+): query: ((?:\d{1,3}\.){3}\d{1,3}|[\_\-A-Za-z0-9]+)\.((?:[\_\-A-Za-z0-9]+[.]*){1,8}) IN (.*) response: ([A-Z]+) \+ .* (\d+ ).*\ (?:[A-Za-z0-9]{1,12})\.(.*);'
  #
  hostIPregex    = regexp_extract('value',pattern, 1).alias('hostIP')
  appregex       = regexp_extract('value',pattern, 2).alias('app')
  appIDregex     = regexp_extract('value',pattern, 3).alias('appID')
  timestampregex = regexp_extract('value',pattern, 4).alias('timestamp')
  clientIPregex  = regexp_extract('value',pattern, 5).alias('clientIP')
  clientIDregex  = regexp_extract('value',pattern, 6).alias('clientID')
  protocolregex  = regexp_extract('value',pattern, 7).alias('protocol')
  hostregex      = regexp_extract('value',pattern, 8).alias('host')
  domainregex    = regexp_extract('value',pattern, 9).alias('domain')
  rFieldregex    = regexp_extract('value',pattern,10).alias('rField')
  levelregex     = regexp_extract('value',pattern,11).alias('level')
  priorityregex  = regexp_extract('value',pattern,12).alias('priority')
  rDomainregex   = regexp_extract('value',pattern,13).alias('rDomain')
  #
  # read the text file
  df1 = spark.read\
             .text(inLog)
  #
  df1 = df1.filter(timestampregex !='')
  #
  df1.persist()
  #
  p1 = createHostsList(df1).select(hostIPregex,\
                                   appregex,\
                                   appIDregex,\
                                   timestampregex,\
                                   clientIPregex,\
                                   clientIDregex,\
                                   protocolregex,\
                                   hostregex,\
                                   domainregex,\
                                   rFieldregex,\
                                   levelregex,\
                                   priorityregex,\
                                   'rHostsList',\
                                   'rHostsNumber')
             
  #
  p1 = p1.repartition(64)
  #
  p1.write.parquet(writePath)
  #