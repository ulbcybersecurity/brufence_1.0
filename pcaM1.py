# list of functions included
#
# runPCA()
# displayPCAModel()
# transformPCAModel()
# displayClusters2d()
# displayClusters3d()
#

#
def runPCA(df,k):
  """
    function runPCA() runs the PCA algorithm on a df
    in order to reduce its dimensions from a d-dimensional feature space
    to a new k-dimensional subspace (k<<d) of k principal components
    with the largest possible variance (in decreasing order)  
  """
  # rational
  #
  pcaModel = PCA(inputCol='scaledFeatures',\
                 outputCol='pcaFeatures',\
                 k=k)\
                 .fit(df)    
  #
  # return value
  return pcaModel
#
# end of runPCA()
#

#
def displayPCAModel(pcaModel):
  """
    function displayPCAModel(pcaModel) displays info about the input pcaModel
  """
  # rational
  pcaMatrix = pcaModel.pc
  print('principal components matrix', pcaMatrix.values)
  print('matrix number of columns:',   pcaMatrix.numCols)
  print('matrix number of rows:',      pcaMatrix.numRows)
  print('Explained variance:',         pcaModel.explainedVariance)
  print('EV values:',                  pcaModel.explainedVariance.values)
#
# end of displayPCAModel()
#
 
#
def transformPCAModel(pcaModel,df):
  """
    function transformPCAModel() transforms a KMeans model using a df
  """
  # local variables
  #
  # rational
  pcaModelT=pcaModel.transform(df)
  #
  # return variable
  return pcaModelT
#
## end of transformPCAModel()
#

#
# functions to display clusters either in 2D or 3D
#
 
#
def displayClusters2d(df,clustersNo):
  """
    function displayClusters2D() reduces the number of dimensions to 2
    markers and colors are defined explicitly
    alternatively, they could be defined implicitly
  """
  # local variables
  markers = ['+','d','^','o','*','v','<','>','s','p','h','x','8','1','2','3']
  #
  colors  = ['blue','crimson','green',\
             'purple','magenta','yellow',\
             'olive','salmon','teal',\
             'gold','indigo','coral',\
             'purple','coral','beige']
  #
  clusters=[]
  #
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_xlabel('X Label')
  ax.set_ylabel('Y Label')
  #
  for i in range(clustersNo):
    clusters.append(df.filter(df.prediction == i))
  #
  for i, cluster in enumerate(clusters):
    group = cluster.select('pcaFeatures').collect()
    subGroup0=[]
    subGroup1=[]
  #
    for j in group:
      subGroup0.append(j[0][0])
      subGroup1.append(j[0][1])
    #
    marker = markers[i]
    color=colors[i]
    label='cluster'+str(i)
    #
    ax.scatter(subGroup0, subGroup1, c = color, marker = marker, label = label)
    #
  #   
  plt.legend()
  plt.show()
#
# end displayClusters2d()
#

#
def displayClusters3d(df,clustersNo):
  # local variables
  markers = ['+','d','^','o','*','v','<','>','s','p','h','x','8','1','2','3']
  #
  colors = ['blue','green','crimson',\
            'purple','magenta','yellow',\
            'olive','salmon','teal',\
            'gold','indigo','coral',\
            'purple','coral','beige']
  #
  clusters=[]
  #
  #
  fig = plt.figure()
  #
  ax = fig.add_subplot(111, projection = '3d')
  ax.set_xlabel('X Label')
  ax.set_ylabel('Y Label')
  ax.set_zlabel('Z Label')
  #
  for i in range(clustersNo):
    clusters.append(df.filter(df.prediction == i))
  #
  
  for i, cluster in enumerate(clusters):
    group = cluster.select('pcaFeatures').collect()
    #
    subGroup0=[]
    subGroup1=[]
    subGroup2=[]
    #
    for j in group:
      subGroup0.append(j[0][0])
      subGroup1.append(j[0][1])
      subGroup2.append(j[0][2])
    #
    marker = markers[i]
    #
    color  = colors[i]
    #
    label='cluster'+str(i)
    #
    ax.scatter(subGroup0, subGroup1, subGroup2, c = color, marker = marker, label = label)  
    #
  #
  plt.title('Clusters in data')
  plt.legend(loc='best')
  plt.show()
#
# end displayClusters3d()
#

