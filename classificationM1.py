# list of functions included
#
# findMaxDistinctCounter()
# defineMClassificationEvaluators()
# runRFClassifier()
# runExtMetricsDFList()
# runExtMetricsDFList2()
# runExtMetricsDFList3()
#

#
def findMaxDistinctCounter(df):
  """
    given a dataset with categorical features:
      1. calculates the maximun number of distint values for all the categorical features
      2. returns this maximum counter
         a. to be used later to set the value for the maxBins parameter
            e.g. of a decision tree or a random forest algorithm 
  """
  # local variables
  max=0
  #
  # rational
  for col in df.columns:
    if (re.match('^idx_',col) and col!='idx_label'):
      tmpMax = df.select(col).distinct().count()
      #
      if (tmpMax>=max):
        max=tmpMax
  #
  # return value
  return max
#
# end of findMaxDistinctCounter()
#

#
def defineMClassificationEvaluators():
  """
    define a MulticlassClassificationEvaluator
    available metrics: accuracy, f1, weightedPrecision, weightedRecall
    the evaluation metric is set to 'accuracy', 'f1', etc.
    using the actual labels and the predictions
    the 'test error' is then calculated equal to 1-accuracy
  """
  # evaluate model's accuracy
  evaluatorAccuracy = MulticlassClassificationEvaluator(labelCol='label',\
                                                        predictionCol='prediction',\
                                                        metricName='accuracy')
  #
  # evaluate model's f1
  evaluatorF1 = MulticlassClassificationEvaluator(labelCol='label',\
                                                  predictionCol='prediction',\
                                                  metricName='f1')
  #
  # evaluate model's weightedPrecision
  evaluatorWP = MulticlassClassificationEvaluator(labelCol='label',\
                                                  predictionCol='prediction',\
                                                  metricName='weightedPrecision')
  #
  # evaluate model's weightedRecall
  evaluatorWR = MulticlassClassificationEvaluator(labelCol='label',\
                                                  predictionCol='prediction',\
                                                  metricName='weightedRecall')
  #
  # return evaluators
  return evaluatorAccuracy,\
         evaluatorF1,\
         evaluatorWP,\
         evaluatorWR
  #
#
## end of defineMClassificationEvaluators()
#

#
def runRFClassifier(df, evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR):
  # local variables
  extEvalMetrics = []
  #
  # calculate the max number of distinct values of all categorical features
  maxCFCounter = findMaxDistinctCounter(df[2])
  #
  trainSet, testSet = splitDataset(df[2], 0.7, 0.3)
  #
  # fit training data to a RandomForestClassifier
  #
  rfModel = RandomForestClassifier(labelCol    = 'label',\
                                   featuresCol = 'scaledFeatures')\
                                   .setMaxBins(maxCFCounter)\
                                   .fit(trainSet)
  #
  # calculate predictions for the random forest model -- a transformer
  predictionsRF=rfModel.transform(testSet)
  # True Positive
  TP = predictionsRF.filter(predictionsRF.label      == 1)\
                    .filter(predictionsRF.prediction == 1)\
                    .count()
  #extEvalMetrics.append(['TP',TP])
  extEvalMetrics.append(TP)
  # True Negative
  TN = predictionsRF.filter(predictionsRF.label      == 0)\
                    .filter(predictionsRF.prediction == 0)\
                    .count()
  #extEvalMetrics.append(['TN',TN])
  extEvalMetrics.append(TN)
  # False Positive
  FP = predictionsRF.filter(predictionsRF.label      == 0)\
                    .filter(predictionsRF.prediction == 1)\
                    .count()
  #extEvalMetrics.append(['FP',FP])
  extEvalMetrics.append(FP)
  # False Negative
  FN = predictionsRF.filter(predictionsRF.label      == 1)\
                    .filter(predictionsRF.prediction == 0)\
                    .count()
  #extEvalMetrics.append(['FN',FN])
  extEvalMetrics.append(FN)
  # True Positive Rate i.e. Sensitivity
  TPR = TP/(TP+FN + 0.0001)
  #extEvalMetrics.append(['TPR',TPR])
  extEvalMetrics.append(TPR)
  # True Negative Rate ie. Specificity
  TNR = TN/(TN+FP + 0.0001)
  #extEvalMetrics.append(['TNR',TNR])
  extEvalMetrics.append(TNR)
  # False Positive Rate
  FPR = FP/(FP+TN + 0.0001)
  #extEvalMetrics.append(['FPR',FPR])
  extEvalMetrics.append(FPR)
  # False Negative Rate = 1 - Specifity
  FNR = 1 - TNR
  #extEvalMetrics.append(['FNR',FNR])
  extEvalMetrics.append(FNR)
  #  
  # calculate accuracy, F1, Precision and Recall
  accuracyRF = evaluatorAccuracy.evaluate(predictionsRF)
  #extEvalMetrics.append(['accuracy',accuracyRF])
  extEvalMetrics.append(accuracyRF)
  #
  f1RF=evaluatorF1.evaluate(predictionsRF)
  #extEvalMetrics.append(['F1',f1RF])
  extEvalMetrics.append(f1RF)
  #
  weightedPrecisionRF = evaluatorWP.evaluate(predictionsRF)
  #extEvalMetrics.append(['Precision',weightedPrecisionRF])
  extEvalMetrics.append(weightedPrecisionRF)
  #
  weightedRecallRF = evaluatorWR.evaluate(predictionsRF)
  #extEvalMetrics.append(['Recall', weightedRecallRF])
  extEvalMetrics.append(weightedRecallRF)
  #
  # return external evaluation metrics
  return extEvalMetrics
#
## end of runRFClassifier()
#


#
def runExtMetricsDFList(dfListIn):
  # local variables
  dfUpdList = []
  metricsList = ['TP','TN','FP','FN','TPR','TNR','FPR','FNR','accuracy','F1','precision','recall']
  #
  evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR = defineMClassificationEvaluators()
  #
  for idx,df in enumerate(dfListIn):
    try:
      metrics = runRFClassifier(df, evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR)
      #
      dfMetrics = spark.createDataFrame([metrics],metricsList)
      #
      dfUpdList.append([df[0],df[1],dfMetrics])
    except:
      pass  
    #
  #
  # return value
  return dfUpdList
  #
#


#
def runExtMetricsDFList2(dfClusters, writePath):
  # local variables
  dfUpdList = []
  metricsList = ['TP','TN','FP','FN','TPR','TNR','FPR','FNR','accuracy','F1','precision','recall']
  #
  evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR = defineMClassificationEvaluators()
  #
  for clustList in dfClusters:
    #
    for clust in clustList[1]:
      dfTmp = spark\
              .read\
              .format('parquet')\
              .load(clust)
      #
      dfName   = clustList[0]
      #
      match = re.search(r'([\_\.\-A-Za-z0-9]+[-]\d{1,2})$',clust)
      index = match.group(0)      
      #
      try:
        metrics = runRFClassifier2(dfTmp, evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR)
        #
        dfMetrics = spark.createDataFrame([metrics],metricsList)
        #
        dfMetrics.write.parquet(writePath + dfName + '/' + index)
        #
        dfUpdList.append([dfName,index,dfMetrics])
      except:
        pass  
      #
  #
  # return value
  return dfUpdList
  #
#

#
def runExtMetricsDFList3(dfListIn, writePath):
  # local variables
  dfUpdList = []
  metricsList = ['TP','TN','FP','FN','TPR','TNR','FPR','FNR','accuracy','F1','precision','recall']
  #
  evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR = defineMClassificationEvaluators()
  #
  for df in dfListIn:
    try:
      print('processing df: ',df[1])
      metrics = runRFClassifier(df, evaluatorAccuracy, evaluatorF1, evaluatorWP, evaluatorWR)
      #
      dfMetrics = spark.createDataFrame([metrics],metricsList)
      #
      match = re.search(r'[\.0-9]+',df[1])
      index = match.group(0)
      #
      fMetrics.write.parquet(writePath + index + '/' + df[1])
      #
      dfUpdList.append([df[0],df[1],dfMetrics])
    except:
      #pass
      print('unable to process df: ', df[1])  
    #
  #
  # return value
  return dfUpdList
  #
#

