# list of functions included
#
# createTestSet()
# splitDataset()
# splitDataset2()
# extractAttrDF()
# extractAttr()
#

#
def createTestSet(kmModelT,clustNum):
  #
  dataT = kmModelT.filter(kmModelT.prediction == 0)\
                  .sample(withReplacement=False, fraction=0.1, seed=9)
  #
  for i in range(1,clustNum):
    dataTmp = kmModelT.filter(kmModelT.prediction == i)\
                      .sample(withReplacement=False, fraction=0.1, seed=9)
    dataT = dataT.union(dataTmp)
  #
  return dataT
#

#  
def splitDataset(df, trainPerc, testPerc):
  #
  trainSet, testSet = df.randomSplit([trainPerc,\
                                      testPerc])
  #
  return trainSet, testSet
  #
#

#  
def splitDataset2(df, trainPerc, testPerc, validatePerc):
  #
  trainSet, testSet, validateSet = df.randomSplit([trainPerc,\
                                                   testPerc,\
                                                   validatePerc])
  #
  return trainSet, testSet, validateSet
  #
#

#
def extractAttrDF(dfList,idx):
  #
  return dfList[idx][1]

#

#
def extractAttr(dfList,idx):
  #
  return dfList[idx][0]

#
