# list of functions included
#
# calcOutliers()
# updLabel1()
# updLabel2()
# createETDFList()
# createClientIPsDFList()
# getEntitiesListInDF()
# createEntitiesListsInDF()
# updAllETGlobalLists()
# initializeEntitiesGlobalLists()
# updETGlobalList()
# unionEntityDFs()
# createEntityDFsAll()
# getNorms()
# getStrongOutliers()
# getWOutliersInCluster()
# getWeakOutliers()
# calcNormsOutliers()
# processDFList2()
# processDFList3()
# getETNormalMixedClusters()
# createFPathsList()
# createFPathsList2()
# listRetrieve()
# getETDetails()
# getETDetails2()
# dispayOutliers()
# dispayOutliers2()
# dispayOutliers3()
#

# 
def calcOutliers(df,outliersNum):
  """
    calculate the top outliersNum outliers
  """
  #
  outliers = df.limit(outliersNum)\
               .filter(df.label == 1)
  #
  return outliers
#

#
def updLabel1(df, clustNum, thrFactor):
  #
  dfClust = df.filter(df.cluster == clustNum)
  #
  meanCDFDistance = dfClust.select(mean(col('cdfDistance')).alias('meanCDFDistance'))\
                           .rdd.map(lambda r: r[0])\
                           .collect()[0]
    #
  stdCDFDistance = df.filter(df.cluster == clustNum)\
                     .select(stddev(col('cdfDistance')).alias('stdCDFDistance'))\
                     .rdd.map(lambda r: r[0])\
                     .collect()[0]
  
  #
  limitUp   = meanCDFDistance + thrFactor * stdCDFDistance
  #
  dfClust = dfClust.withColumn('label',
                               F.when(dfClust.cdfDistance > limitUp, 1)\
                               .otherwise(0))
  #
  return dfClust
#

#
def updLabel2(df, clustNum, thrFactor):
  #
  dfClust = df.filter(df.cluster == clustNum)
  #
  meanCDFDistance = dfClust.select(mean(col('cdfDistance')).alias('meanCDFDistance'))\
                           .rdd.map(lambda r: r[0])\
                           .collect()[0]
    #
  stdCDFDistance = df.filter(df.cluster == clustNum)\
                     .select(stddev(col('cdfDistance')).alias('stdCDFDistance'))\
                     .rdd.map(lambda r: r[0])\
                     .collect()[0]
  
  #
  limitUp   = meanCDFDistance + thrFactor * stdCDFDistance
  #
  limitDown = meanCDFDistance - thrFactor * stdCDFDistance
  #
  dfClust = dfClust.withColumn('label',
                               F.when(dfClust.cdfDistance > limitUp, 1)\
                                .when(dfClust.cdfDistance < limitDown, 1)\
                                .otherwise(0))
  #
  return dfClust
#

#
def createETDFList(df,list, eT):
  """
    function createETDFList() creates a list of dfs using a df, regarding an entity type
  """
  # local variables
  tmpDFList=[]
  #
  # rational
  for idx, elem in enumerate(list):
    #
    dfTmp = df.filter(eT == elem)
    #
    tmpDFList.append([elem,dfTmp]) 
  #
  # return value
  return tmpDFList
# end of createETDFList()
#

#
def createClientIPsDFList(df,list):
  # local variables
  tmpDFList=[]
  #
  # rational
  for idx, elem in enumerate(list):
    #
    dfTmp = df.filter(df.clientIP == elem)
    #
    tmpDFList.append([elem,dfTmp]) 
  #
  # return value
  return tmpDFList
# end of createClientIPsDFList()
#

#
def getEntitiesListInDF(dfIn,eType):
  #
  dfIn[1].columns
  #
  entitiesList = dfIn[1].select(eType)\
                        .distinct()\
                        .rdd\
                        .map(lambda cl:cl[0])\
                        .collect()
  #
  return entitiesList
#

#
def createEntitiesListsInDF(dfIn, entityTypes):
  #
  for eT in entityTypes:
    if eT == 'clientIP':
      clientIPsList = getEntitiesListInDF(dfIn,'clientIP')
    elif eT == 'session':
      sessionsList = getEntitiesListInDF(dfIn,'session')
    elif eT == 'host':
      hostsList = getEntitiesListInDF(dfIn,'host')
    #
  #
  return clientIPsList, sessionsList, hostsList
  #
#

#
def updAllETGlobalLists(dfWaitingList, clientIPsGlobalList, sessionsGlobalList, hostsGlobalList):
  #
  for df in dfWaitingList:
    #
    print(df[0], df[1].columns)
    #
    clientIPsList, sessionsList, hostsList = createEntitiesListsInDF(df,entityTypes)
    #
    clientIPsGlobalList = updETGlobalList(clientIPsGlobalList,clientIPsList)
    sessionsGlobalList  = updETGlobalList(sessionsGlobalList,sessionsList)
    hostsGlobalList     = updETGlobalList(hostsGlobalList,hostsList)
  #
  return clientIPsGlobalList,\
         sessionsGlobalList,\
         hostsGlobalList
  #
#

#
def initializeEntitiesGlobalLists():
  # 
  clientIPsGlobalList = []
  sessionsGlobalList = []
  hostsGlobalList    = []
  #
  return clientIPsGlobalList,\
         sessionsGlobalList,\
         hostsGlobalList
  #
#         
  

#
def updETGlobalList(eTGlobalList,entitiesList):
  #
  eTGlobalListUpd = eTGlobalList + entitiesList
  #
  return eTGlobalListUpd
  #
#

#
##
#
def unionEntityDFs(entity,dfList,eType):
  #
  dfTmp = dfList[0][1].filter(col(eType) == entity)
  #
  for i in range(1,len(dfList)):
    dfTmpNext = dfList[i][1].filter(col(eType) == entity)
    dfTmp = dfTmp.union(dfTmpNext)
    #
  #
  return dfTmp
#

#
def createEntityDFsAll(entityGlobalList,dfList,eType):
  #
  entityDFListAll = []
  #
  for idx, e in enumerate(entityGlobalList):
    print(idx, e)
    entityDFListAll.append([e,unionEntityDFs(e,dfList,eType)])
    print('\n')

  #
  return entityDFListAll
  #
#

#
def getNorms(dfClusters, eT, writePath):
  #
  normalProfiles = []
  #
  for clust in dfClusters:
    #
    normProf = clust[2].filter('label = 0')
    #
    dfName   = clust[1]
    #
    match = re.search(r'[\.0-9]+',dfName)
    index = match.group(0)
    #
    normProf.write.parquet(writePath + index + '/' + dfName)
    #
    normalProfiles.append([clust[0],normProf])
    #
  #
  return normalProfiles
  #
# end of getNorms()
#

#
def getStrongOutliers(dfClusters, eT, writePath):
  #
  strongOutliersList = []
  #
  for clust in dfClusters:
    #
    sOutliers = calcOutliers(clust[2],10)
    #
    dfName   = clust[1]
    #
    match = re.search(r'[\.0-9]+',dfName)
    index = match.group(0)    
    #
    sOutliers.write.parquet(writePath + index + '/' + dfName)
    #
    strongOutliersList.append([clust[0],sOutliers])
    #
  #
  return strongOutliersList
  #
# end of getStrongOutliers()
#

#
def getWOutliersInCluster(clustSchema):
  #
  wOutliersInCluster = clustSchema.filter('label = 1')\
                                  .filter('rankDistance2 >10')
  #
  return wOutliersInCluster
  #
#
# end of getWOutliersInCluster()
#

#
def getWeakOutliers(dfClusters, eT, writePath):
  #
  weakOutliersList = []
  #
  for clust in dfClusters:
    #
    wOutliers = getWOutliersInCluster(clust[2])
    #
    dfName   = clust[1]
    #
    match = re.search(r'[\.0-9]+',dfName)
    index = match.group(0)        
    #
    wOutliers.write.parquet(writePath + index + '/' + dfName)
    #
    weakOutliersList.append([clust[0], wOutliers])
    #
  #
  return weakOutliersList
  #
#
# end of getWeakOutliers()
#

#
def calcNormsOutliers(df, eT, labeledDFsPath, normDFsPath, soDFsPath, woDFsPath):
  #
  dfLabeledClusters         = []
  normalProfiles            = []
  strongOutliersList        = []
  weakOutliersList          = []
  dfLabelledClustersSummary = []
  #
  #
  dfLabeledClusters         = createDFLabeledClusters(df, eT, labeledDFsPath)
  #
  normalProfiles            = getNorms(dfLabeledClusters, eT, normDFsPath)
  #
  strongOutliersList        = getStrongOutliers(dfLabeledClusters, eT, soDFsPath)
  #
  weakOutliersList          = getWeakOutliers(dfLabeledClusters, eT, woDFsPath)
  #
  dfLabelledClustersSummary.append([df[0],\
                                    df[1],\
                                    dfLabeledClusters,\
                                    normalProfiles,\
                                    strongOutliersList,\
                                    weakOutliersList])
  #
  return dfLabelledClustersSummary
  #
#
# end of calcNormsOutliers()
#

#
def processDFList2(entityListAll, eT):
  #
  dfsNormsOutliers = []
  colsOutList      = ['timeStamp','rHostsNumber']
  #
  try:  
    for df in entityListAll:
      #
      print('start processing ',df[0])
      #
      df[1]   = colsToIdx3(df[1],'clientIP', colsOutList)
      df[1]   = createFeaturesVector(df[1])
      df[1]   = stdScalerDF(df[1])
      df[1]   = createTimeFieldsInDF2(df[1],'timeStamp')
      #
      metrics = runEvalKM3(df[1])
      optK    = chooseOptimalK(metrics,0)
      #
      kmModel = runKMeans3(df[1],optK)
      #
      df[1]   = transformKModel(kmModel,df[1])
      df[1]   = calcDistance(df[1],kmModel)
      df[1]   = df[1].withColumnRenamed('prediction','cluster')
      # define window over distance.desc()
      w_distance1 = Window.partitionBy('cluster')\
                          .orderBy(df[1].distance.desc())
      #
      df[1]   = df[1].withColumn('rankDistance1',\
                                 rank().over(w_distance1))
      #
      w_distance2 = Window.partitionBy('cluster')\
                          .orderBy(df[1].distance.asc())
      #
      df[1] = df[1].withColumn('cdfDistance',\
                               cume_dist().over(w_distance2))
      #
      w_distance3 = Window.partitionBy('cluster')\
                          .orderBy(df[1].cdfDistance.desc())
      #
      df[1] = df[1].withColumn('rankDistance2',\
                               rank().over(w_distance3))
      #
      dfsNormsOutliers.append(calcNormsOutliers(df,eT))
      #
      print('end of processing ',df[0])
      #
  #
  except:
    pass
  #
  return dfsNormsOutliers 
#
## end of processDFList2()
#

#
def processDFList3(entityListAll, eT):
  #
  labeledDFsList = []
  colsOutList      = ['timeStamp','rHostsNumber']
  #  
  for df in entityListAll:
  #
    try:
      #
      print('start processing ',df[0])
      #
      df[1]   = colsToIdx3(df[1], 'clientIP', colsOutList)
      df[1]   = createFeaturesVector(df[1])
      df[1]   = stdScalerDF(df[1])
      df[1]   = createTimeFieldsInDF2(df[1],'timeStamp')
      #
      metrics = runEvalKM3(df[1])
      optK    = chooseOptimalK(metrics,0)
      #
      kmModel = runKMeans3(df[1],optK)
      #
      df[1]   = transformKModel(kmModel,df[1])
      df[1]   = calcDistance(df[1],kmModel)
      df[1]   = df[1].withColumnRenamed('prediction','cluster')
      # define window over distance.desc()
      w_distance1 = Window.partitionBy('cluster')\
                          .orderBy(df[1].distance.desc())
      #
      df[1]   = df[1].withColumn('rankDistance1',\
                                 rank().over(w_distance1))
      #
      w_distance2 = Window.partitionBy('cluster')\
                          .orderBy(df[1].distance.asc())
      #
      df[1] = df[1].withColumn('cdfDistance',\
                               cume_dist().over(w_distance2))
      #
      w_distance3 = Window.partitionBy('cluster')\
                          .orderBy(df[1].cdfDistance.desc())
      #
      df[1] = df[1].withColumn('rankDistance2',\
                               rank().over(w_distance3))
      #
      df[1].persist()
      print('new columns have been added')
      df[1].select('cluster').distinct().show()
      #
      labeledDFsList.append(createDFLabeledClusters(df,eT))
      #
      print('end of processing ',df[0])
      #
    #
    except:
      pass
  #
  return labeledDFsList 
#
# end of processDFList3()
#

#
def getETNormalMixedClusters(dfListIn):
  # local variables
  NCList = []
  MCList = []
  #
  for i in dfListIn:
    #
    if i[2].select('label').distinct().count() == 1:
      NCList.append(i[1])
    else:
      MCList.append(i[1]) 
    #
  #
  return NCList, MCList  
#

#
def createFPathsList2(path, MCETList):
  #
  eTFilesList = []
  filesWList  = []
  eTList      = []
  #
  # get entities list given a list of cluster paths
  eTList = getETList(MCETList)
  print(eTList)
  # create fPaths list
  filesWList = createETFPathsList(path, eTList, MCETList)
  print(filesWList)
  # create a list of filePaths for each entity
  eTFilesList = list(zip(eTList,filesWList))
  #
  return eTFilesList
  #
#

#
def createFPathsList(path):
  #
  dirList = []
  fList1  = []
  fList2  = []
  #
  dirPath = path + '*/'
  #
  for dir in glob.glob(dirPath):
    #
    print('processing ',dir)
    fPath = dir + '*'
    #
    match1 = re.search(r'([\.0-9]+[\/])$',dir)
    index1 = match1.group(0)
    #
    match2 = re.search(r'[\.0-9]+',index1)
    index2 = match2.group(0)
    #
    fList2.append([index2, glob.glob(fPath)])
    #
  #
  return fList2
  #
#

#
def listRetrieve(el, listIn):
  #
  lFound = []
  #
  for i in listIn:
    #print(i)
    if i[0] == el:
      #print(el)
      lFound = i[1]
      #
    #
  #
  return lFound
#

#
def getETDetails2(eTList, clIPsNPPaths, clIPsSOPaths, clIPsWOPaths, clIPsMPaths):
  #
  #
  eTPathList = []
  npDFList   = []
  soDFList   = []
  woDFList   = []
  mDFList    = []
  eTDFList   = []
  #
  for e in eTList:
    #
    # retrieve files with the baseline profiles of normal behaviour
    nPList = listRetrieve(e, clIPsNPPaths)
    # retrieve files with the strong outliers
    soList = listRetrieve(e, clIPsSOPaths)
    # retrieve files with the weak outliers
    woList = listRetrieve(e, clIPsWOPaths)
    # retrieve files with the evaluation metrics
    mList  = listRetrieve(e, clIPsMPaths)
    #
    eTPathList.append([e,nPList,soList,woList,mList])
    # 
  #
  print('path lists have been created')
  #
  for p in eTPathList:
    # retrieve dfs of baseline profiles of normal behaviour
    npDFList = parquetToDFList(p[1])
    # retrieve dfs of strong outliers
    soDFList = parquetToDFList(p[2])
    # retrieve dfs of weak outliers
    woDFList = parquetToDFList(p[3])
    # retrieve dfs of evaluation metrics
    mDFList  = parquetToDFList(p[4])
    #
    eTDFList.append([p[0], npDFList, soDFList, woDFList, mDFList])
    #
  #
  return eTDFList
#

#
def getETDetails(pathNorms,   clIPsNPPaths,\
                 pathSOs,     clIPsSOPaths,\
                 pathWOs,     clIPsWOPaths,\
                 pathMetrics, clIPsMsPaths):
  #
  eTList     = []
  eTPathList = []
  npDFList   = []
  soDFList   = []
  woDFList   = []
  mDFList    = []
  eTDFList   = []
  #
  dirPath = pathNorms + '*/'
  #
  for dir in glob.glob(dirPath):
    #
    match1 = re.search(r'([\.0-9]+[\/])$',dir)
    index1 = match1.group(0)
    #
    match2 = re.search(r'[\.0-9]+',index1)
    index2 = match2.group(0)
    #
    if index2 not in eTList:
      eTList.append(index2)
    #
    print('list of entities',eTList)
  #
  for e in eTList:
    #
    # retrieve files with the baseline profiles of normal behaviour
    nPList = listSearch(el, clIPsNPPaths)
    # retrieve files with the strong outliers
    soList = listSearch(el, clIPsSOPaths)
    # retrieve files with the weak outliers
    woList = listSearch(el, clIPsWOPaths)
    # retrieve files with the evaluation metrics
    mList  = listSearch(el, clIPsMsPaths)
    #
    eTPathList.append([e,nPList,soList,woList,mList])
    #
    
  for p in eTPathList:
    #
    # retrieve dfs of baseline profiles of normal behaviour
    npDFList = parquetToDFList(p[1])
    # retrieve dfs of strong outliers
    soDFList = parquetToDFList(p[2])
    # retrieve dfs of weak outliers
    woDFList = parquetToDFList(p[3])
    # retrieve dfs of evaluation metrics
    mDFList  = parquetToDFList(p[4])
    #
    eTDFList.append([p[0], npDFList, soDFList, woDFList, mDFList])
    #
  #
  return eTDFList
#

#
def dispayOutliers(listIn):
  #
  print('clientIP:', listIn[0])
  #
  soAndMetricsList = list(zip(listIn[2],listIn[4]))
  HSFilter  = column('rankDistance2') <= 2
  MSFilter1 = column('rankDistance2')  > 2
  MSFilter2 = column('rankDistance2') <= 4
  LSFilter  = column('rankDistance2') >  4
  #
  for clustNum,i in enumerate(soAndMetricsList):
    print('\nin cluster: ',clustNum)
    so6 = i[0].filter('rankDistance2 <= 6')
    so6Num = so6.count()
    #
    HSOutliers = so6.where(HSFilter)
    print('High Severity Outliers')
    HSOutliers.select('host','domain','hostType','rHostsList','rankDistance2')\
              .show(truncate = False)
    #
    MSOutliers = so6.where(MSFilter1 & MSFilter2)
    if MSOutliers.count()>0:
      print('Medium Severity Outliers')
      MSOutliers.select('host','domain','hostType','rHostsList','rankDistance2')\
                .show(truncate = False)
    #   
    LSOutliers = so6.where(LSFilter)
    if LSOutliers.count()>0:
      print('Low Severity Outliers')
      LSOutliers.select('host','domain','hostType','rHostsList','rankDistance2')\
                .show(truncate = False)
    #
  #   
#

#
def displayOutliers2(listIn):
  #
  print('clientIP:', listIn[0])
  #
  soAndMetricsList = list(zip(listIn[2],listIn[4]))
  HSFilter  = column('rankDistance2') <= 2
  MSFilter1 = column('rankDistance2')  > 2
  MSFilter2 = column('rankDistance2') <= 4
  LSFilter  = column('rankDistance2') >  4
  #
  clustersNum = len(listIn[2]) - 1
  #
  for clustNum,i in enumerate(soAndMetricsList):
    print('\nin cluster: ',clustNum)
    so6 = i[0].filter('rankDistance2 <= 6')
    so6Num = so6.count()
    #
    HSOutliers = so6.where(HSFilter)
    print('High Severity Outliers')
    HSOutliers.select('host','domain','hostType','rHostsList')\
              .show(truncate = False)
    #
    MSOutliers = so6.where(MSFilter1 & MSFilter2)
    if MSOutliers.count()>0:
      print('Medium Severity Outliers')
      MSOutliers.select('host','domain','hostType','rHostsList')\
                .show(truncate = False)
    #   
    LSOutliers = so6.where(LSFilter)
    if LSOutliers.count()>0:
      print('Low Severity Outliers')
      LSOutliers.select('host','domain','hostType','rHostsList')\
                .show(truncate = False)
    #
    if clustNum == 0:
      totalHSOutliers = HSOutliers
      totalMSOutliers = MSOutliers
      totalLSOutliers = LSOutliers
    else:
      totalHSOutliers = totalHSOutliers.union(HSOutliers)
      totalMSOutliers = totalMSOutliers.union(MSOutliers)
      totalLSOutliers = totalLSOutliers.union(LSOutliers)
    #
    if clustNum == clustersNum:
      print('total hs outliers')
      totalHSOutliers.select('host','domain','hostType','rHostsList')\
                     .show(truncate = False)
      #
      if totalMSOutliers.count() > 0:
        print('total ms outliers')
        totalMSOutliers.select('host','domain','hostType','rHostsList')\
                       .show(truncate = False)
      #
      if totalLSOutliers.count() > 0:
        print('total ls outliers')
        totalLSOutliers.select('host','domain','hostType','rHostsList')\
                       .show(truncate = False)
      #
    #  
  #
#

#
def displayOutliers3(listIn):
  #
  print('\n')
  print('clientIP:', listIn[0])
  print('************************')
  #
  soAndMetricsList = list(zip(listIn[2],listIn[4]))
  HSFilter  = column('rankDistance2') <= 2
  MSFilter1 = column('rankDistance2')  > 2
  MSFilter2 = column('rankDistance2') <= 4
  LSFilter  = column('rankDistance2') >  4
  #
  clustersNum = len(listIn[2]) - 1
  #
  for clustNum,i in enumerate(soAndMetricsList):
    #
    so6 = i[0].filter('rankDistance2 <= 6')
    so6Num = so6.count()
    #
    HSOutliers = so6.where(HSFilter)
    #
    MSOutliers = so6.where(MSFilter1 & MSFilter2)
    #   
    LSOutliers = so6.where(LSFilter)
    #
    if clustNum == 0:
      totalHSOutliers = HSOutliers
      totalMSOutliers = MSOutliers
      totalLSOutliers = LSOutliers
      totalMetrics    = i[1]
    else:
      totalHSOutliers = totalHSOutliers.union(HSOutliers)
      totalMSOutliers = totalMSOutliers.union(MSOutliers)
      totalLSOutliers = totalLSOutliers.union(LSOutliers)
      totalMetrics    = totalMetrics.union(i[1])
    #
    if clustNum == clustersNum:
      print('High Severity outliers')
      print('*****************')
      totalHSOutliers.select('host','domain','hostType','rHostsList','timeStamp')\
                     .orderBy(totalHSOutliers.timeStamp.asc())\
                     .show(truncate = False)
      #print('\n')
      #
      if totalMSOutliers.count() > 0:
        print('Medium Severity outliers')
        print('******************')
        totalMSOutliers.select('host','domain','hostType','rHostsList','timeStamp')\
                       .orderBy(totalMSOutliers.timeStamp.asc())\
                       .show(truncate = False)
      #print('\n')
      #
      if totalLSOutliers.count() > 0:
        print('Low Severity outliers')
        print('******************')
        totalLSOutliers.select('host','domain','hostType','rHostsList','timeStamp')\
                       .orderBy(totalLSOutliers.timeStamp.asc())\
                       .show(truncate = False)
      #print('\n')
      #
      totalMetricsCols = totalMetrics.columns
      #
      totalMetricsMean = totalMetrics.select([mean(col).alias('avg'+col) for col in totalMetricsCols])
      #
      print('Average Evaluation Metrics for ', listIn[0])    
      totalMetricsMean.show(truncate = False)
      #            
  #
  print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')   
  #
  return totalMetricsMean
#

#
def displayOutliersMetrics(ETListIN):
  #
  for i, clIP in enumerate(ETListIN):
    #
    totalMetricsMean = func3(clIP)
    #
    if i == 0:
      totaLMetricsAllMean = totalMetricsMean
      #
    else:
      totaLMetricsAllMean = totaLMetricsAllMean.union(totalMetricsMean)
    #
  #
  metricsCols = totaLMetricsAllMean.columns
  #
  print('Average Evaluation Metrics for all ClientIPs')
  totaLMetricsAllMean.select([mean(col).alias(col) for col in metricsCols])\
                     .show(truncate = False)
  #
#



