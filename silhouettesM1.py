# list of functions included
#
# get_silhouettes()
# get_silhouettes2()
# get_silhouettes3()
#

#
def get_silhouettes(kmModelT,clustNum):
  #
  dataT = kmModelT.filter(kmModelT.prediction == 0)
  #dataT = kmModelT.filter(kmModelT.prediction == 0)\
  #                .sample(withReplacement=False, fraction=0.1, seed=9)
  #
  for i in range(1,clustNum):
    dataTmp = kmModelT.filter(kmModelT.prediction == i)
    #dataTmp = kmModelT.filter(kmModelT.prediction == i)\
    #                  .sample(withReplacement=False, fraction=0.1, seed=9)
    dataT = dataT.union(dataTmp)
  
  #
  data = dataT.select('scaledFeatures')\
	          .rdd.map(lambda row : row[0])\
	          .collect()
	             #.take(recsNumToBeProcessed)
  #
  labels = dataT.select('prediction')\
	            .rdd.map(lambda row: row[0])\
	            .collect()
	               #.take(recsNumToBeProcessed)
  #
  clusterLabels = np.unique(labels)
  #
  clustersNum = clusterLabels.shape[0]
  #
  silhouette_values = silhouette_samples(data,\
                                       labels,\
                                       metric='euclidean')
  #
  return dataT, silhouette_values
#



#
def get_silhouettes2(kmModelT,clustNum):
  #
  dataT = kmModelT.filter(kmModelT.prediction == 0)
  #dataT = kmModelT.filter(kmModelT.prediction == 0)\
  #                .sample(withReplacement=False, fraction=0.1, seed=9)
  #
  for i in range(1,clustNum):
    dataTmp = kmModelT.filter(kmModelT.prediction == i)\
                      .sample(withReplacement=False, fraction=0.1, seed=9)
    dataT = dataT.union(dataTmp)
  
  #
  data = dataT.select('pcaFeatures')\
	          .rdd.map(lambda row : row[0])\
	          .collect()
	             #.take(recsNumToBeProcessed)
  #
  labels = dataT.select('prediction')\
	            .rdd.map(lambda row: row[0])\
	            .collect()
	               #.take(recsNumToBeProcessed)
  #
  clusterLabels = np.unique(labels)
  #
  clustersNum = clusterLabels.shape[0]
  #
  silhouette_values = silhouette_samples(data,\
                                       labels,\
                                       metric='euclidean')
  #
  return dataT, silhouette_values
#

#
def get_silhouettes3(kmModelT):
  #
  data =   kmModelT.select('scaledFeatures')\
	               .rdd.map(lambda row : row[0])\
	               .collect()
  #
  labels = kmModelT.select('prediction')\
	            .rdd.map(lambda row: row[0])\
	            .collect()
  #
  clusterLabels = np.unique(labels)
  #
  clustersNum = clusterLabels.shape[0]
  #
  silhouette_values = silhouette_samples(data,\
                                       labels,\
                                       metric='euclidean')
  #
  return silhouette_values
#

