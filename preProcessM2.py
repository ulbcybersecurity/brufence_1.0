# list of functions included
#
# multiple_replace()
# changeOID()
# isRow()
# funIsRow()
# getComplexCol()
# isComplexField()
# findComplexFields()
# flattenDFA()
# getSchemasInMultiNestedDict()
# flattenDFPayloads()
# flattenDFPayloadsA()
# flattenDFPayloadsA2()
# flattenDFPayloadsA3()
# flattenDFPayloadsA4()
# flattenDFPayloadsA5()
# flattenDFPayloadsA6()
# isString()
# isDict()
# extractDict()
# extractDict2()
# extractDict3()
# extractDictsInDFList()
# recTo2D()
# createFilesBasedOnSchemas()
# chooseColsIdxDF()
# chooseColsIdxDFList()
# createFilesFromLog()
# extractSchemasFromDict()
# extractSchemasFromListOfDicts()
# extractSchemaR()
# addSchemaType()
# createDFListFromDF()
# findTimeFieldsInDFA()
# findTimeFieldsInDFsList()
# findDistinctTimeFieldsInDFsList()
# flattenDFTimestampID2()
# flattenDFTimestampID()
# displayGroupsByFieldInDFList()
# joinDFs()
# displayFieldsInDFList()
# createTmpFilesList()
# populateTmpFiles()
# createDF()
# createDFList()
# displayDFListSummary()
# createDFIndexed()
# createDFsListIndexed()
# getYear1()
# getYear2()
# getMonth1()
# getMonth2()
# getMDay1()
# getMDay2()
# getHour1()
# getHour2()
# getMin1()
# getMin2()
# createTimeFieldsInDF1()
# createTimeFieldsInDF2()
#

#
def multiple_replace(text, aDict):
  """
    function multiple_replace() does multiple replacements based on regexs stored in a dictionary
  """
  #
  regEx = re.compile('|'.join(map(re.escape, aDict)))
  #
  def patternFound(match):
    #
    return aDict[match.group(0)]
  # 
  return regEx.sub(paterrnFound, text)
#
# end of multiple_replace()
#
 
#
def changeOID(dfList):
  """
    function changeOID() replaces attribute '$oid' with 'oid' in a list of dfs
  """
  # local variables
  tmpDFListNew = []
  # rational
  for i in range(len(dfList)):
    tmpDFListNew.append(dfList[i].withColumnRenamed('$oid','oid'))
  #
  # return value
  return tmpDFListNew
#
# end of changeOID()
#

#
def isRow(a):
  #
  if type(a) == Row:
    isRow(a[0])
  else:
    print('\t',a, ':',type(a))
#

#
def funIsRow(df):
  #
  #complexField = False
  #
  for c in df.columns:
    rec = df.select(col(c)).first()[0]
    print('rec: ',rec)
    isRow(rec)
    #
  #
#    

#
def getComplexCol(dfIn,row,field):
  #
  depth = 0
  tmpDict = {}
  colName = ''
  #
  if type(row) == Row:
    depth += 1
    getComplexCol(dfIn,row[0],field)
  #
  if depth == 1:
    tmpDict = json.loads(dfIn.select(to_json(field))\
                  .limit(1)\
                  .collect()[0][0])
  elif depth == 2:
    tmpDict = json.loads(dfIn.select(to_json(field))\
                  .limit(1)\
                  .collect()[0][0][0])  
  #
  for k in tmpDict.keys():
    colName = k
  #
  return colName
#

# 
def isComplexField(a):
  #
  isComplex = False
  #
  if type(a) == Row:
    isComplex = True
  #
  return isComplex
#      

#
def findComplexFields(df):
  #
  fieldsList = []
  #
  for c in df.columns:
    att = df.select(col(c)).first()[0]
    if isComplexField(att):
      fieldsList.append(c)
    #
  #
  return fieldsList
#

#
def flattenDFA(df,fieldsList):
  """
    function flattenDFA() flattens each complex struct field
  """
  #
  dfNew = df
  timeField = ''
  #
  for c in df.columns:
    att = df.select(col(c)).first()[0]
    if isComplexField(att):
      #
      field = c + '_'
      #
      complexField = '$.' + getComplexCol(df,att,c)
      #
      dfNew = dfNew.withColumn(field,get_json_object(to_json(c),complexField))
      #
      if field.startswith('timestamp'):
        timeField = field
        dfNew = dfNew.withColumn('dateOut', unix_timestamp(timeField, "yyyy-MM-dd'T'HH:mm:ss.SSSz").cast('timestamp'))
        dfNew = dfNew.drop(timeField)      
      #
      dfNew = dfNew.drop(c)
      #
  #
  return dfNew  
#

#
def getSchemasInMultiNestedDict(dfIn,dictAttr):
  """
    extract the schemas of a multi-nested dictionary as a list
  """
  #
  return [[idx,schema[0]] \
         for idx,schema in extractSchemasFromListOfDicts\
         ([p[0] for p in dfA1.select('payload')\
                             .collect()]).items()]
#

#
def flattenDFPayloads(dfList):
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPayloads[idx][1]
    #
    for att in schema:
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))\
                   .withColumn('id',get_json_object(to_json('_id'),'$.$oid'))
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload' and col !='_id'])  
  #
## end of function flattenDFPayloads()
#

#
def flattenDFPayloadsA(dfList,schemasInPloads):
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))\
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])  
  #
# end of function flattenDFPayloadsA()
#

#
def flattenDFPayloadsA2(dfList,schemasInPloads):
  #
  #timeFieldsList = []
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
    #
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))
      #
      if att in findDistinctTimeFieldsInDFsList(dfList):
        df[1] = df[1].withColumn('dateIn', unix_timestamp(att, "yyyy-MM-dd'T'HH:mm:ss.SSSz").cast('timestamp'))
        df[1] = df[1].drop(att)
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])  
  #
# end of function flattenDFPayloadsA2()
#

#
def flattenDFPayloadsA3(dfList,schemasInPloads):
  #
  timeFieldsList = []
  dateFormats = ['yyyy/MM/dd HH:mm:ss', "yyyy-MM-dd'T'HH:mm:ss.SSSSz", "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"]
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
    #
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))
    #
    timeFieldsList = findTimeFieldsInDFA(df[1])
    #
    for t in timeFieldsList:
      #          
      if t != 'dateOut':
        newTimeField = t + 'In'
        #
        for dFormat in dateFormats:
          flag = False
          #
          countValidDates = df[1].select(to_timestamp(t,dFormat).alias('t1'))\
                       .filter(col('t1').isNotNull())\
                       .count()
          #
          if countValidDates > 0:
            flag = True
          #
          if flag:
            df[1] = df[1].withColumn(newTimeField, unix_timestamp(col(t), dFormat).cast('timestamp'))
            break
          else:
            pass    
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])    
    #
  #
# end of function flattenDFPayloadsA3()
#

#
def flattenDFPayloadsA4(dfList,schemasInPloads):
  #
  timeFieldsList = []
  dateFormats = ['yyyy/MM/dd HH:mm:ss', "yyyy-MM-dd'T'HH:mm:ss.SSSz", "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd HH:mm:ss"]
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
    #
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))
    #
    timeFieldsList = findTimeFieldsInDFA(df[1])
    #
    #
    for t in timeFieldsList:
      #          
      if t != 'dateOut':
        newTimeField = t + 'In'
        #
        for dFormat in dateFormats:
          flag = False
          #
          countValidDates = df[1].select(to_timestamp(t,dFormat).alias('t1'))\
                                 .filter(col('t1').isNotNull())\
                                 .count()
          #
          if countValidDates > 0:
            flag = True
            df[1] = df[1].withColumn(newTimeField, unix_timestamp(col(t), dFormat).cast('timestamp'))
            df[1] = df[1].drop(t)
          #
          if flag:
            break
          else:
            pass    
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])    
    #
  #
# end of function flattenDFPayloadsA4()
#

#
def flattenDFPayloadsA5(dfList,schemasInPloads):
  #
  timeFieldsList = []
  dateFormats = ['yyyy/MM/dd HH:mm:ss', "yyyy-MM-dd'T'HH:mm:ss.SSSz", "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd HH:mm:ss"]
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
    #
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))
    #
    timeFieldsList = findTimeFieldsInDFA(df[1])
    #
    for t in timeFieldsList:
      #          
      if t != 'dateOut':
        newTimeField = t + 'In'
        #        
        for dFormat in dateFormats:
          flag = False
          #
          countValidDates = df[1].select(to_timestamp(t,dFormat).alias('t1'))\
                                 .filter(col('t1').isNotNull())\
                                 .count()
          #
          if countValidDates > 0:
            flag = True
            df[1] = df[1].withColumn(newTimeField, unix_timestamp(col(t), dFormat).cast('timestamp'))
            df[1] = df[1].drop(t)
          #
          if flag:
            break
          else:
            pass    
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])    
    #
# end of function flattenDFPayloadsA5()
#

#
#
def flattenDFPayloadsA6(dfList,schemasInPloads):
  #
  timeFieldsList = []
  dateFormats = ['yyyy/MM/dd HH:mm:ss', "yyyy-MM-dd'T'HH:mm:ss.SSSz", "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd HH:mm:ss"]
  #
  for idx,df in enumerate(dfList):
    #
    schema = schemasInPloads[idx][1]
    #
    for att in schema:
    #
      df[1] = df[1].withColumn(att,json_tuple(df[1].payload,att))
    #
    timeFieldsList = findTimeFieldsInDFA(df[1])
    #
    for t in timeFieldsList:
      #          
#      if t != 'dateOut':
        newTimeField = t + 'In'
        #        
        for dFormat in dateFormats:
          flag = False
          #
          countValidDates = df[1].select(to_timestamp(t,dFormat).alias('t1'))\
                                 .filter(col('t1').isNotNull())\
                                 .count()
          #
          if countValidDates > 0:
            flag = True
            df[1] = df[1].withColumn(newTimeField, unix_timestamp(col(t), dFormat).cast('timestamp'))
            df[1] = df[1].drop(t)
          #
          if flag:
            break
          else:
            pass    
    #
    df[1] = df[1].select([col for col in df[1].columns if col != 'payload'])    
    #
# end of function flattenDFPayloadsA6()
#

#
def isString(rec):
  """
    function isString() checks if a rec or an attribute is a string
  """
  #
  if type(rec) is str:
    return True
  else:
    return False  
#
# end of isString()
#

#
def isDict(rec):
  """
    function isDict() checks if a rec or an attribute is a dict
  """
  #
  if type(json.loads(rec)) is dict:
    return True
  else:
    return False  
#
# end of isDict()
#

#
def extractDict(dfIn):
#
  for c in dfIn.columns:
    firstRec = dfIn.select(c).limit(1).collect()[0][0]
    try:
      if type(firstRec) is str:
        if type(json.loads(firstRec)) is dict:
          localSchema = extractSchemaR(json.loads(firstRec))
        #
      #
      else:
        pass
    except ValueError:
      pass
    #
  #
#
# end of extractDict()
#

#
def extractDict2(dfIn):
  #
  dfNew = dfIn
  for c in dfIn.columns:
    #
    try:
      firstRec = dfIn.select(c).limit(1).collect()[0][0]
      #
      if isString(firstRec):
        if isDict(firstRec):
          print('field:',c)
          print(firstRec)
          localSchema = extractSchemaR(json.loads(firstRec))
          print('local schema:',localSchema)
          print('\n')
        #
          for att in localSchema:
            print('adding field:',att)
            dfNew = dfNew.withColumn(att,json_tuple(col(c),att))
          #
          dfNew = dfNew.drop(c)
      #
      else:
        pass
    except ValueError:
      pass
    #
  #
  return dfNew
#
# end of extractDict2()
#

#
def extractDict3(dfIn):
  """
    function extractDict3() extracts all fields from a dict
  """
  #
  dfNew = dfIn
  for c in dfIn.columns:
    #
    try:
      firstRecValue = dfIn.select(c).first()[0]
      #
      if isString(firstRecValue):
        if isDict(firstRecValue):
          localSchema = extractSchemaR(json.loads(firstRecValue))
        #
          for att in localSchema:
            dfNew = dfNew.withColumn(att,json_tuple(col(c),att))
          #
          dfNew = dfNew.drop(c)
      #
      else:
        pass
    except EOFError or ValueError:
      continue
    #
  #
  return dfNew
#
# end of extractDict3()
#

#
def extractDictsInDFList(dfListIn):
  #
  dfListOut = []
  #
  for df in dfListIn:
    dfOut = extractDict3(df[1])
    dfListOut.append([df[0],dfOut])
  #
  return dfListOut
#


#
def recTo2D(rec):
  """
    function recTo2D() converts the input record to a 2D dictionary
  """
  #  
  recFlat={}
  #
  fieldName=''
  #
  def returnList(key,aList):
    if aList==[]:
      recFlat[key]=''
    else:
      for index,field in enumerate(aList):
        if type(field) is list:
          returnList(key,field)
        elif type(field) is dict:
          returnDict(key,field)
        else:
          fieldName=key+str(index)
          recFlat[fieldName]=field
  #
  def returnDict(key,aDict):
    if aDict=={}:
      recFlat[aDict]={}
    else:
      for k,v in aDict.items():
        if type(v) is dict:
          returnDict(k,v)
        elif type(v) is list:
          returnList(k,v)
        else:
          recFlat[k]=v
  #
  for k,v in rec.items():
    if isinstance(v, dict):
      returnDict(k,v)
    elif isinstance(v,list):
      returnList(k,v)
    else:
      recFlat[k]=v
  #
  return recFlat
  #
# end of recTo2D()
#

#
def createFilesBasedOnSchemas(aSchemasList):
  for i in range(len(aSchemasList)):
    outF=os.path.join(path,'f'+str(i))
    #
    f=open(outF,'w')

#

#
def chooseColsIdxDF(df):
  # local variables
  dfCols = []
  # rational
  for col in df.columns:
    if re.match('^idx_',col) and\
               (col!='idx_label'):
      dfCols.append(col)
  #
  # return value
  return dfCols
#
# end of chooseColsIdxDF()
#  

#
def chooseColsIdxDFList(dfList):
  # local variables
  dfColsList=[]
  # rational
  for i,df in enumerate(dfList):
    dfColsList.append(chooseColsIdxDF(df))
  #
  # return value
  return dfColsList
#
# end of chooseColsIdxDFList()
#

#
def createFilesFromLog(fileIn):
  """
    function createFilesFromLog() creates tmp files
    after splitting the original input log using the inner schemas
  """
  # define local variables
  schemas=defaultdict(list)
  tmpFilesList=[]
  #
  # func rational
  schemas=extractSchemasFromDict(fileIn)
  tmpFilesList=createTmpFilesList(schemas)
  populateTmpFiles(fileIn,schemas)
  # return value
  return tmpFilesList
#
# end of createDFsFromLog()
# 

#
def extractSchemasFromDict(fileIn):
  """
    extract a Schema from an input file
    schema is in the form of a dict
  """
    indexS=0
    schemasDict=defaultdict(list)
    #
    with open(fileIn,'r') as csvF:
      for index,line in enumerate(csvF):
        lineF=recTo2D(json.loads(multiple_replace(line,kwsDict)))
        tSchema=extractSchemaR(lineF)
        #
        if [tSchema] not in schemasDict.values():
          schemasDict[indexS].append(tSchema)
          indexS +=1
    
    csvF.close()
    #
    return schemasDict
    #
# end of extractSchemasFromDict()
#

#
def extractSchemasFromListOfDicts(listIn):
    #
    indexS=0
    schemasDict=defaultdict(list)    
    for index,line in enumerate(listIn):
      try:
        lineF=json.loads(listIn[index])
      except ValueError:
        continue
      #  
      tSchema=extractSchemaR(lineF)
      #
      if [tSchema] not in schemasDict.values():
        schemasDict[indexS].append(tSchema)
        indexS +=1
    
    # 
    return schemasDict
#
# end of extractSchemasFromListOfDicts()
#

#
def extractSchemaR(aDict):
  tempSchema=[]
  for k,v in aDict.items():
    tempSchema.append(k)
  return tempSchema
#

#
def addSchemaType(df,schemasDict):
  #
  def getSchema(p):
  #
    sType = '0'
    #
    try:
      s1=extractSchemaR(json.loads(p))
      #
      for s2 in schemasDict:
        if s2[1] == s1:
          sType = s2[0]
          break
      #
    except ValueError:
      pass
    #
    return sType
  # 
  udfGetSchema = udf(getSchema)
  #
  dfNew = df.withColumn('schemaType',udfGetSchema(col("payload")))
  #
  return dfNew
#

#
def createDFListFromDF(df, numSchemas, prefix):
  # local variables
  tmpDFList=[]
  #
  # rational
  for idx in range(numSchemas):
    postIdx = ''
    #
    if idx < 10:
      postIdx = str(0) + str(idx)
    #  
    else:
      postIdx = str(idx)
    #  
    dfName = prefix + postIdx
    #
    tmpDict=dict([(dfName,dfName)])
    #
    for key, v in tmpDict.items():
      v = df.filter(df.schemaType == idx)
    #
    tmpDFList.append([dfName,v]) 
  #
  # return value
  return tmpDFList
# end of createDFListFromDF()
#

#
def findTimeFieldsInDFA(df):
  # local variables
  timeFieldsList=[]
  #
  # rational
  for c in df.columns:
      if(c.startswith('$date') or\
         c.startswith('timestamp') or\
         c.startswith('date') or\
         c.endswith('date') or\
         c.endswith('timestamp') or\
         c.endswith('date_') or\
         c.endswith('timestamp_') or\
         c.endswith('time') or\
         c.endswith('Time')
         ):
        #print('time field found:',c)
        timeFieldsList.append(c)
  #
  # return value
  return timeFieldsList      
#
# end of findTimeFieldsInDF()
#

#
def findTimeFieldsInDFsList(dfListIn):
  # local variables
  timeFieldsListInDFs=[]
  #
  # rational
  for i,df in enumerate(dfListIn):
    timeFieldsListInDFs.append(findTimeFieldsInDFA(df[1]))
  #
  # return value
  return timeFieldsListInDFs
#
# end of findTimeFieldsInDFsList()
#

#
def findDistinctTimeFieldsInDFsList(dfListIn):
  # local variables
  timeDistinctFieldsListInDFs=[]
  #
  # rational
  for i,df in enumerate(dfListIn):
    tfList = findTimeFieldsInDFA(df[1])
    #
    for tf in tfList:
      #
      if tf not in timeDistinctFieldsListInDFs:
        timeDistinctFieldsListInDFs.append(tf)
    #
  #
  # return value
  return timeDistinctFieldsListInDFs
#
# end of findDistinctTimeFieldsInDFsList()
#

#
def flattenDFTimestampID2(df):
  #
  df = df.withColumn('id',get_json_object(to_json('_id'),'$.$oid'))\
         .withColumn('timestampDate',get_json_object(to_json('timestamp'),'$.$date'))
  #
  df = df.select('id','timestampDate','ident','channel')
  #
  return df
# end of function flattenDFTimestampID2(()
#

#
def flattenDFTimestampID(df):
  #
  df = df.withColumn('id',get_json_object(to_json('_id'),'$.$oid'))\
         .withColumn('ts',get_json_object(to_json('timestamp'),'$.$date'))\
         .withColumn('timestampDate', unix_timestamp('ts', "yyyy-MM-dd'T'HH:mm:ss.SSSz").cast('timestamp'))
  #
  df = df.select('id','timestampDate','ident','channel')
  #
  return df
# end of function flattenDFTimestampID(()
#

#
def displayGroupsByFieldInDFList(dfList):
  #
  for df in dfList:
    print('dataframe :',df[0], df[1])
    #
    for col in df[1].columns:
      #print('col)
      df[1].groupBy(col).count().show(truncate=False)
    #
    print('end of groupings for dataframe ',df[0])
    print('\n')
    #
  #
#

#
def joinDFs(df, dfList):
  # local variables
  tmpDFList = []
  #
  # rational
  #
  for d in dfList:
    d[1] = d[1].join(df,'id','inner')
  #
# end of joiDFs()
#

#
def displayFieldsInDFList(dfList):
  #
  for df in dfList:
    print('dataframe :',df[1])
    #
    for col in df[1].columns:
      print(col)
    #
    print('\n')
    #
  #
# end of displayFieldsInDFList()
#

#
def createTmpFilesList(schemasDict):
    """
      function createTmpFilesList() creates a list of files using a dict of different schemas
      each schema corresponds to a file
    """
    # define local variables
    tmpFList=[]
    #
    # rational
    for idx in range(len(schemasDict)):  
      outF=os.path.join(path,'f'+str(idx))
      tmpFList.append(outF)
      f=open(outF,'w')
      f.close()
    #
    # return value
    return tmpFList
#
# end of createTmpFilesList()
#

# 
def populateTmpFiles(fileIn,schemasDict):
  """
    function populateTmpFiles() populates a file with raw data
    following a schema in a dict of schemas
  """
  # rational
    with open(fileIn,'r') as csvF:
      for line in csvF.readlines():
        lineF=recTo2D(json.loads(multiple_replace(line,kwsDict)))
        tSchema=extractSchemaR(lineF)
        for index,aSchema in schemasDict.items():
          # check for a schema and write each corresponded rec into the file
          if [tSchema]==aSchema:
            writeF=os.path.join(path,'f'+str(index))
            outF=open(writeF,'a')
            outF.write(json.dumps(lineF)+'\n')
            outF.close()
            break

# end of populateTmpFiles()
#

#
def createDF(aFile):
    """
      function createDF() creates the df of a file
    """
    # func rational
    match=re.search(r'\d{1,2}$',aFile)
    index=match.group(0)
    dfName='df'+index
    tmpDict=dict([(dfName,dfName)])
    for key,v in tmpDict.items():
      v=spark.read.json(aFile)
    #del tmpDict
    # return value
    return v

# end of createDF()
#

#
def createDFList(fileList):
  """
    function createDFList() creates a list of dfs using a list of files
  """
  # local variables
  tmpDFList=[]
  #
  # rational
  for aFile in fileList:
      tmpDFList.append(createDF(aFile)) 
  #
  # return value
  return tmpDFList

# end of createDFList()
#

#
def displayDFListSummary(dfList):
  """
    display a summary of a list of dfs
  """
  # rational
  for i,df in enumerate(dfList):
    print('df',i,\
          'number of columns:',len(df.columns),\
          'number of rows:',df.count(),\
          '\n',\
          df,\
          '\n\n')
#
# end of displayDFListSummary()
#

#
def createDFIndexed(df):
  # local variables
  #
  # rational
  dfIndexed=colsToIdx(df)
  #
  # return value
  return dfIndexed
#
# end of createDFIndexed()
#

#
def createDFsListIndexed(dfList):
  # local variables
  dfsListIndexed=[]
  # 
  # rational
  for i,df in enumerate(dfList):
    dfsListIndexed.append(createDFIndexed(df))
  #
  # return value
  return dfsListIndexed
#
# end of createDFsListIndexed()
#

#
def getYear1(timestamp):
  # return value
  return time.strptime(timestamp,'%Y-%m-%dT%H:%M:%S.%f%z').tm_year
#
# end of getYear()
#

#
def getYear2(timestamp):
  # return value
  return str(year(timestamp))
#
# end of getYear()
#

#
def getMonth1(timestamp):
  # rational
  month = time.strptime(timestamp,'%Y-%m-%dT%H:%M:%S.%f%z').tm_mon
  #
  # add an extra '0' in the head of the return value if its less than 10
  if int(month)<10:
    month = str(0) + str(month)
  #
  # return value
  return month
#
# end of getMonth()
#

#
def getMonth2(timestamp):
  # rational
  monthV = month(timestamp)
  #
  # add an extra '0' in the head of the return value if its less than 10
  if monthV < 10:
    monthS = str(0) + str(monthV)
  #
  # return value
  return monthS
#
# end of getMonth()
#

#
def getMDay1(timestamp):
  # rational
  mDay = time.strptime(timestamp,'%Y-%m-%dT%H:%M:%S.%f%z').tm_mday
  #
  # add an extra '0' in the head of the return value if its less than 10
  if int(mDay)<10:
    mDay = str(0) + str(mDay)
  #
  # return value
  return mDay
#
# end of getMDay()
#

#
def getMDay2(timestamp):
  # rational
  mDayV = dayofmonth(timestamp)
  #
  # add an extra '0' in the head of the return value if its less than 10
  if mDayV < 10:
    mDayS = str(0) + str(mDayV)
  #
  # return value
  return mDayS
#
# end of getMDay()
#

#
def getHour1(timestamp):
  # rational
  hour = time.strptime(timestamp,'%Y-%m-%dT%H:%M:%S.%f%z').tm_hour
  #  
  # add an extra '0' in the head of the return value if its less than 10  
  if int(hour)<10:
    hour = str(0) + str(hour)
  #
  # return value
  return hour
#
#  end of getHour()
#

#
def getHour2(timestamp):
  # rational
  hourV = hour(timestamp)
  #  
  # add an extra '0' in the head of the return value if its less than 10  
  if hourV < 10:
    hourS = str(0) + str(hourV)
  #
  # return value
  return hourS
#
#  end of getHour()
#

#
def getMin1(timestamp):
  # rational
  min = time.strptime(timestamp,'%Y-%m-%dT%H:%M:%S.%f%z').tm_min
  #
  # add an extra '0' in the head of the return value if its less than 10  
  if int(min)<1:
    min = str(0) + str(min)
  #  
  # return value
  return min
#
# end of getMin()
#

#
def getMin2(timestamp):
  # rational
  minV = minute(timestamp)
  #
  # add an extra '0' in the head of the return value if its less than 10  
  if minV < 10:
    minS = str(0) + str(minV)
  #  
  # return value
  return minS
#
# end of getMin()
#

#
def createTimeFieldsInDF1(df):
  # local variables
  # 
  # rational
  #
  # extract the time fields of a timestamp using udfs
  udfGetMonth=udf(getMonth,StringType())
  udfGetYear=udf(getYear,StringType())
  udfGetMDay=udf(getMDay,StringType())
  udfGetHour=udf(getHour,StringType())
  udfGetMin=udf(getMin,StringType())
  #
  # create new fields for the year, month, mDay, hour and min of a timestamp field of a df
  dfWithTS=df.withColumn('year',udfGetYear('$date'))\
             .withColumn('month',concat('year',udfGetMonth('$date')))\
             .withColumn('mDay',concat('month',udfGetMDay('$date')))\
             .withColumn('hour',concat('mDay',udfGetHour('$date')))\
             .withColumn('min',concat('hour',udfGetMin('$date')))
  #
  # return value
  return dfWithTS
#
# end of createTimeFieldsInDF()
#  

#
def createTimeFieldsInDF2(df,dateAttr):
  # local variables
  # 
  # rational
  monthS = when(F.month(dateAttr)<10, concat(lit('0'), F.month(dateAttr).cast('string')))\
          .otherwise(F.month(dateAttr).cast('string'))
  #
  mDayS = when(F.dayofmonth(dateAttr)<10, concat(lit('0'), F.dayofmonth(dateAttr).cast('string')))\
          .otherwise(F.dayofmonth(dateAttr).cast('string'))
  #
  hourS = when(F.hour(dateAttr)<10, concat(lit('0'), F.hour(dateAttr).cast('string')))\
          .otherwise(F.hour(dateAttr).cast('string'))
  #
  minuteS = when(F.minute(dateAttr)<10, concat(lit('0'), F.minute(dateAttr).cast('string')))\
          .otherwise(F.minute(dateAttr).cast('string')) 
  # create new fields for the year, month, mDay, hour and min of a timestamp field of a df
  dfWithTS=df.withColumn('year',year(dateAttr).cast('string'))\
             .withColumn('month',concat('year',monthS))\
             .withColumn('mDay',concat('month',mDayS))\
             .withColumn('hour',concat('mDay',hourS))\
             .withColumn('minute',concat('hour',minuteS))
  #
  # return value
  return dfWithTS
#
# end of createTimeFieldsInDF2()
#


