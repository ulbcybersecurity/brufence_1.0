# list of functions included
#
# dfColRename()
# preProcessDF()
# getHosts()
# createHostsList()
# getListLength()
# getSingletonFields(
# removeSingleValueFields()
# createTimeFieldsInDF2()
# createTimespaceGroupsTSDF()
# createListTSDF()
# createYearList()
# createMonthList()
# createMDayList()
# createHourList()
# createMinuteList()
# createTSDFList1()
# createTSDFList2()
# colsToIdx()
# colsToIdx2()
# colsToIdx3()
# chooseIdxCols()
# createFeaturesVector()
# createDFIndexed2()
# stdScalerDF()
# minMaxScalerDF()
# normalizeDF()
#

#
def dfColRename(df):
  #
  df = df.withColumnRenamed('rField',  'hostType')\
         .withColumnRenamed('level',   'responseLevel')\
         .withColumnRenamed('clientID','session')\
         .withColumn('timeStamp', unix_timestamp('timestamp', "dd-MMM-yyyy HH:mm:ss.SSS").cast('timestamp'))
  #
  return df
  #
#

#
def preProcessDF(df):
  #
  df[1] = dfColRename(df[1])
  df[1] = removeSingleValueFields(df[1],getSingletonFields(df[1])) 
  #
  return df
#

#
def getHosts(str):
  """
    function getHosts() returns all users/hosts recorded e.g. by kerberos in a single event
  """
  # rational
  pattern = '[ ]([\.A-Za-z0-9]+)[\.]{0,1};'
  #
  hosts = re.findall(pattern,str)
  #  
  # return value
  return hosts
#

#
def createHostsList(df):
  """
    function createHostsList() creates a list of the rHosts and add them as a new column in a df
  """
  # local variables
  # 
  # rational
  #
  # extract users/hosts in an event, using a udf
  udfGetHosts=udf(getHosts,StringType())
  # calculate the length of a list, using a udf
  udfGetListLength=udf(getListLength,IntegerType())
  # create new column for a list of users/hosts in a df, for each event
  dfWithHostsList=df.withColumn('rHostsList',udfGetHosts('value'))\
                    .withColumn('rHostsNumber',udfGetListLength('rHostsList'))
  #
  # return value
  return dfWithHostsList
#
# end of createHostsList()
#  

#
def getListLength(list):
  """
    function getListLength() returns the length of a list column
  """
  # rational
  listLength = len(list)
  #  
  # return value
  return listLength
#

#
def getSingletonFields(dfIn):
  """
    function getSingletonFields() returns a list of all single-value attributes
  """
  #
  attList = []
  #
  for c in dfIn.columns:
    if dfIn.select(c).distinct().count() == 1:
      attList.append(c) 
  #
  return attList
  #
#

#
def removeSingleValueFields(dfIn,attList):
  """
    function removeSingleValueFields() removes all single-value columns from a df
  """
  #
  attList2 = [c for c in dfIn.columns if c not in attList]
  #
  dfOut = dfIn.select([c for c in attList2])
  #
  return dfOut
  #
#

#
def createTimeFieldsInDF2(df,dateAttr):
  """
    function createTimeFieldsInDF() creates new time fields extracted from a timestamp field of a df
  """
  # local variables
  # 
  # rational
  monthS = when(F.month(dateAttr)<10, concat(lit('0'), F.month(dateAttr).cast('string')))\
          .otherwise(F.month(dateAttr).cast('string'))
  #
  mDayS = when(F.dayofmonth(dateAttr)<10, concat(lit('0'), F.dayofmonth(dateAttr).cast('string')))\
          .otherwise(F.dayofmonth(dateAttr).cast('string'))
  #
  hourS = when(F.hour(dateAttr)<10, concat(lit('0'), F.hour(dateAttr).cast('string')))\
          .otherwise(F.hour(dateAttr).cast('string'))
  #
  minuteS = when(F.minute(dateAttr)<10, concat(lit('0'), F.minute(dateAttr).cast('string')))\
          .otherwise(F.minute(dateAttr).cast('string')) 
  # create new fields for the year, month, mDay, hour and min of a timestamp field of a df
  dfWithTS=df.withColumn('year',  F.year(dateAttr).cast('string'))\
             .withColumn('month', concat('year',monthS))\
             .withColumn('mDay',  concat('month',mDayS))\
             .withColumn('hour',  concat('mDay',hourS))\
             .withColumn('minute',concat('hour',minuteS))
  #
  # return value
  return dfWithTS
#
# end of createTimeFieldsInDF2()
#

#
def createTimespaceGroupsTSDF(dfTS,timespace):
  """
    function createTimespaceGroupsTSDF() returns all time-space groups in a given time-series
  """
  # local variables
  timespaceGroups=[]
  #
  # rational
  timespaceGroupsDF=dfTS.groupBy(timespace)\
                        .count()\
                        .collect()
  #
  for i in timespaceGroupsDF:
    timespaceGroups.append([i[0],i[1]])
  #
  #
  # return value
  return timespaceGroups
#
# end of createTimespaceGroupsTSDF()
#  

#
def createListTSDF(dfTS,timespace):
  """
    function createListTSDF() creates a list of dfs given a dfTS and a timespace e.g. year, month, etc.
  """
  # local variables
  listTimespaceDFs=[]
  #
  # rational
  print('create the list of dfs given a dfTS and a time space \n')
  print('\n timespace:',timespace) 
  #
  timespaceGroups=createTimespaceGroupsTSDF(dfTS,timespace)
  #
  for i,v in enumerate(timespaceGroups):
    dfName = timespace + 'DF'+ v[0]
    tmpDict=dict([(dfName,dfName)])
    #
    for key,v1 in tmpDict.items():
      v1 = dfTS.filter(dfTS[timespace]==v[0])
    
    listTimespaceDFs.append([dfName,\
                             v1])
  #
  # return value
  return listTimespaceDFs
#
# end of createListTSDF()
#  

#
def createYearList(dfTS):
  #
  return createListTSDF(dfTS,'year')
#

#
def createMonthList(dfTS):
  #
  return createListTSDF(dfTS,'month')
#

#
def createMDayList(dfTS):
  #
  return createListTSDF(dfTS,'mDay')
#

#
def createHourList(dfTS):
  #
  return createListTSDF(dfTS,'hour')
#

#
def createMinuteList(dfTS):
  #
  return createListTSDF(dfTS,'minute')
#

#
def createTSDFList2(dfTS):
  # local variables
  yearList = []
  monthList = []
  mDayList = []
  hourList = []
  #minuteList = []
  timespaceList=['year','month','mDay','hour','minute']
  #
  # rational 
  for ts in timespaceList:
    #print(ts)
    if ts == 'year':
      print('year')
      yearList = createYearList(dfTS)
    elif ts == 'month':
      print('month')
      monthList = createMonthList(dfTS)
    elif ts == 'mDay':
      print('mDay')
      mDayList = createMDayList(dfTS)
    elif ts == 'hour':
      print('hour')
      hourList = createHourList(dfTS)
    elif ts == 'minute':
      print('minute')
      minuteList = createMinuteList(dfTS)      
  #
  # return value
  return yearList,\
         monthList,\
         mDayList,\
         hourList,\
         minuteList
#
# end of createTSDFList2()
#  

#
def createTSDFList1(dfTS):
  # local variables
  tsDFsList     = []
  timespaceList = ['year','month','mDay','hour','minute']
  #
  # rational 
  for ts in timespaceList:
    print('\n create the ts list for: ',ts,'\n')
    tsDFsList.append(createListTSDF(dfTS,ts))
  #
  # return value
  return tsDFsList
#
# end of createTSDFList1()
#

#
def colsToIdx(df, colsOutList):
  # local variables
  tmpDF  = df
  inCols = []
  #
  # rational
  for col in df.columns:
    if col not in colsOutList:
      tmpDF = StringIndexer(inputCol=col,\
                            outputCol='idx_' + col)\
                            .fit(df)\
                            .transform(tmpDF)
  #
  for col in tmpDF.columns:
    if col.startswith('idx_'):
      inCols.append(col)
  
  #
  assembler = VectorAssembler(inputCols = inCols,\
                              outputCol = 'features')
  #
  tmpDF = assembler.transform(tmpDF)
  #
  # return value
  return tmpDF
  #
#
# end of colsToIdx()  
#

#
def colsToIdx2(df, colsOutList):
  # local variables
  tmpDF  = df
  inCols = []
  #
  # rational
  for col in df.columns:
    if (col not in colsOutList:
      tmpDF = StringIndexer(inputCol=col,\
                            outputCol='idx_' + col)\
                            .fit(df)\
                            .transform(tmpDF)
  
  #
  # return value
  return tmpDF
  #
#
# end of colsToIdx2()  
#

#
def colsToIdx3(df, attr, colsOutList):
  # local variables
  tmpDF  = df
  inCols = []
  #
  # rational
  for col in df.columns:
    if (col not in colsOutList and\
        col != attr
       ):
      tmpDF = StringIndexer(inputCol=col,\
                            outputCol='idx_' + col)\
                            .fit(df)\
                            .transform(tmpDF)
  # return value
  return tmpDF
  #
#
# end of colsToIdx3()  
#

#
def chooseIdxCols(df):
  # local variables
  dfCols = []
  # rational
  for col in df.columns:
    if re.match('^idx_',col) and\
               (col!='idx_label'):
      dfCols.append(col)
  
  #
  # return value
  return dfCols
#
# end of chooseIdxCols()
# 

#
## function createFeaturesVector()
#
def createFeaturesVector(df):
  # local variables
  #
  # rational
  idxCols=chooseIdxCols(df)
  #
  assembler = VectorAssembler(inputCols = idxCols,\
                              outputCol = 'features')
  #
  tmpDF = assembler.transform(df)
  # return value
  return tmpDF
#
# end of createFeaturesVector()
#

#
def createDFIndexed2(df):
  """
    function createDFIndexed2() indexes a df
  """
  # local variables
  #
  # rational
  dfIndexed=colsToIdx2(df)
  #
  # return value
  return dfIndexed
#
# end of createDFIndexed2()
#

#
def stdScalerDF(dfIndexed):
  """
    function stdScalerDFDF() scales the 'features' column of an indexed df
    standard deviation is set to 'True'
    the 'withMean' parameter is 'False' by default, but here we set it to 'True'
  """
  # rational
  dfIndexedStdScaled = StandardScaler(inputCol  = 'features',\
                                      outputCol = 'scaledFeatures',\
                                      withStd   = True,\
                                      withMean  = True)\
                                      .fit(dfIndexed)\
                                      .transform(dfIndexed)
  #
  # return value
  return dfIndexedStdScaled
#
# end of stdScalerDF()
#

#
def minMaxScalerDF(dfIndexed):
  # rational
  dfIndexedMinMaxScaled = MinMaxScaler(inputCol  = 'features',\
                                       outputCol = 'scaledFeatures1')\
                                       .fit(dfIndexed)\
                                       .transform(dfIndexed)
  #
  # return value
  return dfIndexedMinMaxScaled
#
# end of minMaxScalerDF()
#

#
def normalizeDF(dfIndexed):
  """
    function normalizeDF() normalises the 'features' column of an indexed df,
    with the default p=1.0
  """
  # rational
  dfIndexedNormalized=Normalizer(p = 1.0,\
                                 inputCol="scaledFeatures1",\
                                 outputCol="scaledFeatures")\
                                 .transform(dfIndexed)
  #
  # return value
  return dfIndexedNormalized
#
# end of normalizeDF()
#