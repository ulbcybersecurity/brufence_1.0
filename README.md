# What is BruFence
BruFence is an open source platform which processes and analyses network logs in order to perform anomaly detection. Ultimately, it aims at detecting zero-day attacks and APT (advanced persistent threats) by means of unsupervised Machine Learning.

# Licence
BruFence is available under the GPL3.0 licence : [https://www.gnu.org/licenses/gpl-3.0.html]()

# Status of the repo
We are in the process of cleaning the code. As of today, the code is not usable "out-of-the-box" but we aim at that in a near future. This is a very first commit

# Credits
Brufence is an INNOVIRIS project, funded by Brussels' region (2015-2018), in collaboration with 

- ULB Machine Leanring Group (Prof. Gianluca Bontempi )
- ULB QualSec Group (Prof. Olivier Markowitch)
- UCL Machine Leanring Group (Prof. Marco Saerens)

This code has been developped by Dr. Dimitris Sisiaridis.

Please, cite as:

*Sisiaridis, D., Carcillo, F., & Markowitch, O. (2016, November 10). A Framework for Threat Detection in Communication Systems. Proceedings of the 20th ACM Panhellenic Conference on Informatics (pp. 1-6). ACM Press.*




#Website
http://www.securit-brussels.be/project/brufence/ 



