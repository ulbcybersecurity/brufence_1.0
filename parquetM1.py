# list of functions included
#
# parquetToDF()
# parquetToDF2()
# parquetToDF3()
# parquetToDFList()
# createDFList()
# createDFList2()
# createDFList3()
# createDFList4()
# getMixedDFsList()
#

#
def parquetToDF(aParquetFileFullPath):
    # func rational
    match1 = re.search(r'\d{1,8}[.].+$', aParquetFileFullPath)
    match2 = re.search(r'^\d{1,8}',match1.group(0))
    index1 = match2.group(0)
    match3 = re.search(r'[a-z]+$',match1.group(0))
    index2 = match3.group(0)
    #
    dfName='df' + index1 + index2
    tmpDict=dict([(dfName,dfName)])
    #
    for key,v in tmpDict.items():
      v = spark.read\
               .format('parquet')\
               .load(aParquetFileFullPath)
    
    # return value
    return [key, v]
    #
# end of parquetToDF()
#

#
def parquetToDF2(aParquetFileFullPath):
    # func rational
    match1 = re.search(r'([\_\.\-A-Za-z0-9]+[-]\d{1,2})$', aParquetFileFullPath)
    #
    index1 = match1.group(0)
    #
    dfName = index1
    tmpDict=dict([(dfName,dfName)])
    for key,v in tmpDict.items():
      v = spark.read\
               .format('parquet')\
               .load(aParquetFileFullPath)
    
    #
    # return value
    return [key, v]
    #
# end of parquetToDF2()
#

#
def parquetToDF3(f):
  #
  dfTmp = spark\
          .read\
          .format('parquet')\
          .load(f)
  #
  return dfTmp
  #
#

#
def parquetToDFList(fList):
  #
  dfList = []
  #
  print('start processing ,', fList)
  for f in fList:
    #
    print('processing file:',f)
    dfList.append(parquetToDF3(f))
    #
  #
  return dfList
  #
#

#
def createDFList(pathName):
  #
  filesWList = []
  dfList = []
  fullPathName = pathName + '*/[0-9]*.*'
  #
  for fName in glob.glob(fullPathName):
    filesWList.append(fName)
  # 
     
  for f in filesWList:
    #
    try:
      print('processing file ',f)
      dfTmp = parquetToDF(f)
      dfTmp = preProcessDF(dfTmp)
      dfList.append(dfTmp)
      #
    #
    except:
      #
      print('cannot process file ',f) 
  #
  return dfList
#

#
def createDFList2(pathName):
  #
  filesWList = []
  dfList = []
  fullPathName = pathName + '*/*'
  #
  for fName in glob.glob(fullPathName):
    filesWList.append(fName)
  # 
     
  for f in filesWList:
    #
    try:
      print('processing file ',f)
      dfTmp = parquetToDF2(f)
      dfList.append(dfTmp)
      #
    #
    except:
      #
      print('cannot process file ',f) 
  #
  return dfList
#

#
def createDFList3(pathName):
  #
  filesWList = []
  dfList = []
  fullPathName = pathName + '*'
  #
  for fName in glob.glob(fullPathName):
    filesWList.append(fName)
  # 
     
  for f in filesWList:
    #
    match1 = re.search(r'[\.0-9]+$',f)
    index1 = match1.group(0)
    #
    try:
      print('processing file ',f)
      dfTmp = spark\
              .read\
              .format('parquet')\
              .load(f)
      #
      dfList.append([index1,dfTmp])
      #
    #
    except:
      #
      print('cannot process file ',f) 
  #
  return dfList
#

#
def createDFList4(path,eT):
  #
  filesWList = []
  eTList = []
  dfList = []
  fullPath = path + '*/*'
  #
  print(fullPath)
  for fName in glob.glob(fullPath):
    filesWList.append(fName)
  #    
  for f in filesWList:
    #
    match1 = re.search(r'([\_\.\-A-Za-z0-9]+[-]\d{1,2})$',f)
    index1 = match1.group(0)
    match2 = re.search(r'[\.0-9]+',index1)
    index2 = match2.group(0)
    #
    try:
      print('processing file ',f)
      dfTmp = spark\
              .read\
              .format('parquet')\
              .load(f)
      if index2 not in eTList:
        eTList.append(index2)
      #  
      
      dfList.append([eT, index1, dfTmp])
      #
    except:
      print('cannot process file ',f) 
  #
  return eTList, dfList
#

#
def getMixedDFsList(clustETListIn, dfLDfETList):
  #
  mixedDFsList = []
  #
  for i in clustETListIn:
    for j in dfLDfETList:
      if i == j[1]:
        mixedDFsList.append(j)
      #
    #
  #
  return mixedDFsList      
#
